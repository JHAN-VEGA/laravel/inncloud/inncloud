@php
    $filters = '';
    foreach ($data->query as $key => $value ) {
        if ($key !== 'page') $filters = $filters."&$key=$value";
    }
@endphp
<ul class="pagination">
    @if ($data->currentPage > 1)
        <li class="page-item">
            <a class="page-link text-lightblue" title="Anterior" href="?page={{$data->currentPage - 1}}">
                <i class="fas fa-angle-left"></i>
            </a>
        </li>
    @else
        <li class="page-item disabled">
            <span class="page-link">
                <i class="fas fa-angle-left"></i>
            </span>
        </li>
    @endif

    @for ($i = $data->startPage; $i <= $data->endPage; $i++)
        @if ($i === $data->currentPage)
            <li class="page-item bg-lightblue">
                <span class="page-link bg-lightblue">
                    {{ $i }}
                </span>
            </li>
        @else
            <li class="page-item">
                <a class="page-link text-lightblue" href="?page={{ $i . $filters}}">
                    {{ $i }}
                </a>
            </li>
        @endif
    @endfor

    @if ($data->currentPage < $data->totalPages)
        <li class="page-item">
            <a class="page-link text-lightblue" href="?page={{ $data->currentPage + 1 . $filters}}">
                <i class="fas fa-angle-right"></i>
            </a>
        </li>
    @else
        <li class="page-item disabled">
            <span class="page-link disabled">
                <i class="fas fa-angle-right"></i>
            </span>
        </li>
    @endif
</ul>
