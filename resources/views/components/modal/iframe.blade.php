<style>

</style>

<div class="modal fade {{ $classes ?? 'iframeModal' }}" id="{{ $id ?? 'iframeModal' }}"
     tabindex="-1" aria-hidden="true"  style="z-index: 99999;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            @if(isset($title) && trim($title) !== '')
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{ $title ?? '' }}</h4>
                </div>
            @else
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                        style="position: fixed; z-index: 9999; right: 30px; top: 10px;"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            @endif
            <div class="modal-body">
                <iframe src=""
                        style="width: 100%; border: none;"
                ></iframe>
            </div>
        </div>
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

