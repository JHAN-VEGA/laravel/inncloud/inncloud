@extends('adminlte::page')

@section('plugins.summernote', true)

@section('title', 'Crear')

@section('content_header')
    <h1>Crear documento</h1>
@stop

@section('content')
    <div id="app">
        <form method="POST" action="{{route('documents.store')}}" id="formStore">
            <div class="card card-info card-outline rounded-0">
                <div class="card-header">
                    <label>Fornulario de creación de documentos</label>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">* Nombre</label>
                                <input class="form-control form-control-border" name="name" id="name">
                                <small class="text-danger" validation-for="name"></small>
                            </div>
                            <div class="form-group">
                                <label for="documentType">* Tipo de documento: </label>
                                <select class="form-control" id="documentType" name="document_type_id" style="width: 100%">
                                    <option v-for="(item, index) in documentTypes" :value="item.id">
                                        @{{ item.name + ' (' + item.code +  ')' }}
                                    </option>
                                </select>
                                <small class="text-danger" validation-for="document_type_id"></small>
                            </div>
                            <div class="form-group">
                                <label for="process">* Proceso: </label>
                                <select class="form-control" id="process" name="process_id" style="width: 100%">
                                    <option v-for="(item, index) in processes" :value="item.id">
                                        @{{ item.name + ' (' + item.code +  ')' }}
                                    </option>
                                </select>
                                <small class="text-danger" validation-for="process_id"></small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="body">* Contenido</label>
                                <textarea class="summernote form-control form-control-border" id="body" name="body"
                                          placeholder="Contenido del documento" style="height: 300px"
                                ></textarea>
                                <small class="text-danger" validation-for="body"></small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-flat bg-lightblue">Crear documento</button>
                </div>
            </div>
        </form>
    </div>

@stop

@section('css')

@stop

@section('js')
    <script>
        Vue.createApp({
            data() {
                return {
                    documentTypes: [],
                    processes: [],
                }
            },
            methods: {
                async getdocumentTypes() {
                    fetch('{{route('documentTypes.getAll')}}', {
                        headers: {
                            'Accept': 'application/json',
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        method: 'GET',
                    })
                        .then((res) => res.json())
                        .then((res) => {
                            if (res.success)
                                this.documentTypes = res.data
                        })
                        .catch((err) => {
                            ResponseUtility.processFailFetchRequest(res)
                            $(this).unloading()
                        })
                },
                async getProcesses() {
                    fetch('{{route('process.getAll')}}', {
                        headers: {
                            'Accept': 'application/json',
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        method: 'GET',
                    })
                        .then((res) => res.json())
                        .then((res) => {
                            if (res.success)
                                this.processes = res.data
                        })
                        .catch((err) => {
                            ResponseUtility.processFailFetchRequest(res)
                            $(this).unloading()
                        })
                }
            },
            mounted() {
                this.getdocumentTypes()
                this.getProcesses()
            }
        }).mount('#app')

        $(document).ready(function () {
            $("#documentType").select2({
                theme:'bootstrap4',
            })

            $("#process").select2({
                theme:'bootstrap4',
            })

            $('.summernote').summernote({
                lang: 'es-ES',
                placeholder: 'Contenido del documento',
                tabsize: 2,
                height: 300
            });


            $("#formStore").submit(function (e) {
                e.preventDefault()

                const form = $(this);
                const data = new FormData(document.getElementById('formStore'));

                $(form).loading()

                fetch(form.attr('action'), {
                    headers: {
                        "Accept": "application/json",
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    method: form.attr('method'),
                    body: data
                })
                    .then((res) => res.json())
                    .then((res) => {
                        if (res.success){
                            Swal.fire({
                                title: "Exitoso",
                                text: res.message,
                                icon: "success",
                                confirmButtonText: "Aceptar"
                            }).then((res) => {
                                $(form).unloading();
                                location.reload();
                            })
                        }
                        else{
                            ResponseUtility.processFailFetchRequest(res)
                            $(form).unloading();
                        }
                    })
                    .catch((err) => {
                        ResponseUtility.processFailFetchRequest(err)
                        button.unloading()
                    })

            });
        })

    </script>
@stop
