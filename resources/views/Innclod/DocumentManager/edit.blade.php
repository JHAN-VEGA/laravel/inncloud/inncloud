@extends('adminlte::page')

@section('plugins.summernote', true)

@section('title', 'Editar')

@section('content_header')
    <h1>Editar documento</h1>
@stop

@section('content')
    <div id="app">
        <form method="POST" action="{{route('documents.update', $data->id)}}" id="formUpdate">
            <div class="card card-info card-outline rounded-0">
                <div class="card-header">
                    <label>Fornulario de actualización de documentos ({{ $data->code }})</label>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">* Nombre</label>
                                <input class="form-control form-control-border" name="name" id="name" value="{{$data->name}}">
                                <small class="text-danger" validation-for="name"></small>
                            </div>
                            <div class="form-group">
                                <label for="documentType">* Tipo de documento: </label>
                                <select class="form-control" id="documentType" style="width: 100%" disabled>
                                    <option selected>
                                        {{ $data->documentTypeName . ' (' . $data->documentTypeCode . ')' }}
                                    </option>
                                </select>
                                <small class="text-danger" validation-for="document_type_id"></small>
                            </div>
                            <div class="form-group">
                                <label for="process">* Proceso: </label>
                                <select class="form-control" id="process" style="width: 100%" disabled>
                                    <option selected>
                                        {{ $data->processName . ' (' . $data->processCode . ')' }}
                                    </option>
                                </select>
                                <small class="text-danger" validation-for="process_id"></small>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="body">* Contenido</label>
                                <textarea class="summernote form-control form-control-border" id="body" name="body"
                                          placeholder="Contenido del documento" style="height: 300px"
                                >{!! $data->body !!}</textarea>
                                <small class="text-danger" validation-for="body"></small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-flat bg-lightblue">Actualizar documento</button>
                </div>
            </div>
        </form>
    </div>

@stop

@section('css')

@stop

@section('js')
    <script>
        $(document).ready(function () {
            $("#formUpdate").submit(function (e) {
                e.preventDefault()

                const form = $(this);
                const data = new FormData(document.getElementById('formUpdate'));

                $(form).loading()

                fetch(form.attr('action'), {
                    headers: {
                        "Accept": "application/json",
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    },
                    method: form.attr('method'),
                    body: data
                })
                    .then((res) => res.json())
                    .then((res) => {
                        if (res.success){
                            Swal.fire({
                                title: "Exitoso",
                                text: res.message,
                                icon: "success",
                                confirmButtonText: "Aceptar"
                            }).then((res) => {
                                $(form).unloading();
                                location.reload();
                            })
                        }
                        else{
                            ResponseUtility.processFailFetchRequest(res)
                            $(form).unloading();
                        }
                    })
                    .catch((err) => {
                        ResponseUtility.processFailFetchRequest(err)
                        button.unloading()
                    })
            });

            $('.summernote').summernote({
                lang: 'es-ES',
                placeholder: 'Contenido del documento',
                tabsize: 2,
                height: 300
            });
        })
    </script>
@stop
