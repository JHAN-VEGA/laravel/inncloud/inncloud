@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Listado de documentos</h1>
@stop

@section('content')
    <div class="d-flex justify-content-end">
        <a href="{{ route('documents.index') }}" class="text-lightblue" title="Limpiar filtros">
            <i class="fas fa-retweet"></i> Limpiar filtros
        </a>
    </div>

    <div class="card card-outline card-lightblue rounded-0">
        <div class="card-header">
            <div class="d-flex justify-content-between">
                <label><i class="fas fa-list"></i> Listado de documentos</label>
                <a data-toggle="iframeModal" data-modal-target="#viewerGeneralIframe"
                   data-url="{{ route('documents.create') }}?embebed=1" href="#"
                   class="btn btn-sm btn-flat bg-lightblue" title="Crear documento">
                    Crear documento
                </a>
            </div>
        </div>
        <div class="card-body">

            <form>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <select class="form-control select2 rounded-0" name="searchBy" style="width: 100%">
                            <option value="name">Filtrar por nombre</option>
                            <option value="code">Filtrar por código</option>
                        </select>
                    </div>

                    <div class="col-md-6">
                        <input class="form-control rounded-0" name="value">
                    </div>

                    <div class="col-md-2">
                        <button type="submit" class="btn btn-flat btn-block bg-lightblue"><i class="fas fa-search"></i> Filtrar</button>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-sm">
                    <thead>
                        <tr>
                            <th style="min-width: 50px;">ID</th>
                            <th style="min-width: 100px">Nombre</th>
                            <th style="min-width: 100px">Código</th>
                            <th style="min-width: 100px">Tipo de documento</th>
                            <th style="min-width: 100px">Proceso</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($data->items as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->code}}</td>
                            <td>{{$item->documentTypeName}} ({{$item->documentTypeCode}})</td>
                            <td>{{$item->processName}} ({{$item->processCode}})</td>
                            <td>
                                <form action="{{route('documents.delete', $item->id)}}" method="post">
                                    @csrf
                                    <a data-toggle="iframeModal" data-modal-target="#viewerGeneralIframe"
                                       data-url="{{ route('documents.edit', $item->id) }}?embebed=1" href="#"
                                       class="text-lightblue" title="Editar documento">
                                        <i class="fas fa-edit"></i>
                                    </a>

                                    <button type="submit" class="btn text-danger"><i class="fas fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <x-paginator :data="$data"></x-paginator>

        </div>
    </div>

    <x-modal.iframe classes="Modal iframeModal fullscreen width-80 no-padding" id="viewerGeneralIframe" />

@stop

@section('css')

@stop

@section('js')
@stop
