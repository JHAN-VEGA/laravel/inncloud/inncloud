<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Innclod - Inicio de sesión</title>

    <link rel="stylesheet" href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/adminlte.min.css') }}">

    <link rel="icon" href="{{asset('img/logo.ico')}}">

    <style>
        .gradient-custom-2 {
            background: #fccb90;
            background: -webkit-linear-gradient(to right, #F92015, #FF9F09);
            background: linear-gradient(to right, #F92015, #FF9F09);
        }

        @media (min-width: 768px) {
            .gradient-form {
                height: 100vh !important;
            }
        }
        @media (min-width: 769px) {
            .gradient-custom-2 {
                border-top-right-radius: .3rem;
                border-bottom-right-radius: .3rem;
            }
        }
    </style>
</head>
<body>


<section class="h-100 gradient-form" style="background-color: #eee;">
    <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col-xl-10">
                <div class="card rounded-3 text-black">
                    <div class="row g-0">
                        <div class="col-lg-6">
                            <div class="card-body p-md-5 mx-md-4">

                                <div class="text-center my-5 pb-1">
                                    <img src="{{asset('img/innclod.png')}}"
                                         style="width: 185px;" alt="logo">
                                </div>

                                <form method="post" action="{{route('auth')}}" id="formLogin">
                                    @csrf
                                    <div class="form-group mb-4">
                                        <input type="email" id="email" class="form-control form-control-border @error('email') is-invalid @enderror"
                                               placeholder="correo@innclod.com" name="email"
                                               value="{{old('email')}}"/>
                                        @error('email')
                                        <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>

                                    <div class="form-group mb-4">
                                        <input type="password" id="password" class="form-control form-control-border @error('password') is-invalid @enderror"
                                               name="password" placeholder="Contraseña"/>
                                        @error('password')
                                        <small class="text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>

                                    <div class="text-center pt-1 mb-5 pb-1">
                                        <button id="btnLogin" class="btn btn-block btn-flat fa-lg gradient-custom-2 mb-3 text-white"
                                                type="submit">Iniciar sesión
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>
                        <div class="col-lg-6 d-flex align-items-center gradient-custom-2">
                            <div class="text-white px-3 py-4 p-md-5 mx-md-4">
                                <h4 class="mb-4 text-center text-bold">Innclod</h4>
                                <p class="mb-0">Aplicación para la gestion interna de procesos de Innclod.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('vendor/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('vendor/jquery/jqueryUtility.js') }}"></script>
<script>
    $("#formLogin").submit(function () {
        $(this).loading()
    });
</script>


<!-- Innclod.AuthManager.login -->
</body>
</html>
