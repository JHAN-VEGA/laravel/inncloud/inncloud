<?php

return [
    App\Providers\AppServiceProvider::class,
    AuthManager\Infrastructure\ServiceLayer\AuthManagerServiceProvider::class,
    DocumentManager\Infrastructure\ServiceLayer\DocumentManagerServiceProvider::class,
    Master\Infrastructure\ServiceLayer\MasterServiceProvider::class,
];
