<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('PRO_PROCESO', function (Blueprint $table) {
            $table->bigIncrements('PRO_ID');
            $table->string('PRO_NOMBRE', 60);
            $table->string('PRO_PREFIJO', 20)->unique();
            $table->integer('user_who_created_id')->nullable();
            $table->integer('user_who_updated_id')->nullable();
            $table->integer('user_who_deleted_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::connection('pgsql')
            ->table('PRO_PROCESO')
            ->insert([
                [
                    'PRO_NOMBRE' => 'PENDIENTE',
                    'PRO_PREFIJO' => 'PEN'
                ],
                [
                    'PRO_NOMBRE' => 'DESARROLLO',
                    'PRO_PREFIJO' => 'DES'
                ],
                [
                    'PRO_NOMBRE' => 'PRUEBAS',
                    'PRO_PREFIJO' => 'PRU'
                ],
                [
                    'PRO_NOMBRE' => 'APROBADA',
                    'PRO_PREFIJO' => 'APR'
                ],
                [
                    'PRO_NOMBRE' => 'AJUSTES',
                    'PRO_PREFIJO' => 'AJU'
                ],
                [
                    'PRO_NOMBRE' => 'CERRADA',
                    'PRO_PREFIJO' => 'CER'
                ],
            ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('PRO_PROCESO');
    }
};
