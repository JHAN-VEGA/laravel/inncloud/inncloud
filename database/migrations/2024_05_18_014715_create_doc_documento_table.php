<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('DOC_DOCUMENTO', function (Blueprint $table) {
            $table->bigIncrements('DOC_ID');
            $table->string('DOC_NOMBRE', 50);
            $table->string('DOC_CODIGO', 255)->unique();
            $table->text('DOC_CONTENIDO');
            $table->integer('DOC_ID_TIPO');
            $table->integer('DOC_ID_PROCESO');
            $table->foreign('DOC_ID_TIPO')->references('TIP_ID')->on('TIP_TIPO_DOC');
            $table->foreign('DOC_ID_PROCESO')->references('PRO_ID')->on('PRO_PROCESO');
            $table->integer('user_who_created_id')->nullable();
            $table->integer('user_who_updated_id')->nullable();
            $table->integer('user_who_deleted_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('DOC_DOCUMENTO');
    }
};
