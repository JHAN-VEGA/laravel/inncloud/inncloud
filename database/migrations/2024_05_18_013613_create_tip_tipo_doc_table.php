<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('TIP_TIPO_DOC', function (Blueprint $table) {
            $table->bigIncrements('TIP_ID');
            $table->string('TIP_NOMBRE', 60);
            $table->string('TIP_PREFIJO', 20)->unique();
            $table->integer('user_who_created_id')->nullable();
            $table->integer('user_who_updated_id')->nullable();
            $table->integer('user_who_deleted_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::connection('pgsql')
            ->table('TIP_TIPO_DOC')
            ->insert([
                [
                    'TIP_NOMBRE' => 'MANUAL',
                    'TIP_PREFIJO' => 'MAN'
                ],
                [
                    'TIP_NOMBRE' => 'INSTRUCTIVO',
                    'TIP_PREFIJO' => 'INS'
                ],
                [
                    'TIP_NOMBRE' => 'PROCESO',
                    'TIP_PREFIJO' => 'PRO'
                ],
                [
                    'TIP_NOMBRE' => 'GUÍA',
                    'TIP_PREFIJO' => 'GU'
                ],
                [
                    'TIP_NOMBRE' => 'FASES',
                    'TIP_PREFIJO' => 'FA'
                ],
            ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('TIP_TIPO_DOC');
    }
};
