# Innclod
**Autor:** Jhan Sebastian Vega
**Correo:** [jhanvega01@outlook.com](mailto:jhanvega01@outlook.com)

## Despliegue
- Copiar el archivo docker-compose.override.example.yml con el nombre de docker-compose.override.yml ubicda en la carpeta .devops/docker/develop

- Ajustar los puertos del servidor de Nginx y Postgres que desea exponer

- Crear la red de docker
    ```bash
    docker network create innclod-network
    ```
  
- Antes de levantar los contenedores se tienen que configurar las valiables de entorno
  * Copiar archivo .env.example
  * La conexión a la base de datos está conectada al contenedor innclod-postgres, si cambio parte de este tiene que revisar sus nuevas credenciales 

- Construir la imagen se ejecuta el siguiente comando
    ```bash
    docker-compose -f .devops/docker/develop/docker-compose.yml -f .devops/docker/develop/docker-compose.override.yml build
    ```

- Levantar los contenedores se ejecuta el siguiente comando
  ```bash
  docker-compose -f .devops/docker/develop/docker-compose.yml -f .devops/docker/develop/docker-compose.override.yml up
  ```

- Cuando quiera dejar de exponer los puertos puede ejecutar el siguiente comando
  ```bash
  docker-compose -f .devops/docker/develop/docker-compose.yml -f .devops/docker/develop/docker-compose.override.yml down
  ```

- Para acceder al bash del contenedor se usa el siguiente comando
  ```bash
  docker exec -it innclod-app bash
  ```

- Dentro del contenedor ejecutar el comando para instalar las dependencias de los paquetes
    ```bash
    composer install
    ```

- Una vez dentro del contenedor ejucutar las migraciones, este le creara las tablas necesarias
    ```bash
    php artisan migrate
    ```

- Dentro del contenedor generar key de la aplicación  
    ```bash
    php artisan key:generate
    ```

## Credenciales

**Correo:** admin@innclod.com

**Contraseña:** innclod123
