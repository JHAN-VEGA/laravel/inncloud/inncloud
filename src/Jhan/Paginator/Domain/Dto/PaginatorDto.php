<?php

namespace Jhan\Paginator\Domain\Dto;

use Jhan\Kernel\Domain\Dto\BaseDto;

class PaginatorDto extends BaseDto
{
    public string $path;
    public array $items = [];
    public int $perPage;
    public int $totalItems;
    public int $totalPages;
    public int $currentPage;
    public int $startPage;
    public int $endPage;
    public array $query = [];
}
