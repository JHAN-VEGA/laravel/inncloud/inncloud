<?php

namespace Jhan\Paginator\Infrastructure\Interfaces;

use Jhan\Paginator\Domain\Dto\PaginatorDto;

interface BasePaginatorInterface
{
    public function initialize():self;

    public function paginate(
        int $recordsByPage = 20,
        string $orderBy = 'id',
        string $order = 'desc'
    ):PaginatorDto;

}
