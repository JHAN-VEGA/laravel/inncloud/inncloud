<?php

namespace Jhan\Paginator\Infrastructure\Paginators;

use Illuminate\Database\Query\Builder;
use Jhan\Paginator\Application\Mappers\PaginatorDtoMapper;
use Jhan\Paginator\Domain\Dto\PaginatorDto;
use Jhan\Paginator\Infrastructure\Interfaces\BasePaginatorInterface;

abstract class BasePaginator implements BasePaginatorInterface
{
    protected bool $isMapItems = false;

    protected ?Builder $query = null;

    abstract protected function getTableName():string;

    abstract protected function getDatabaseConnection():string;

    abstract public function initialize():self;

    public function paginate(
        int $recordsByPage = 20,
        string $orderBy = 'id',
        string $order = 'desc'
    ): PaginatorDto
    {
        $records = $this->query
            ->orderBy($orderBy, $order)
            ->paginate($recordsByPage)
            ->appends(request()->query());

        $paginatorDto = (new PaginatorDtoMapper())
            ->createFromPaginate($records);

        if ($this->isMapItems) {
            $paginatorDto->items = $this->mapItems($paginatorDto->items);
        }

        return $paginatorDto;
    }

    protected function mapItems(array $records):array
    {
        return $records;
    }
}
