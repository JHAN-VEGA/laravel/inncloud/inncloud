<?php

namespace Jhan\Paginator\Application\Mappers;

use Illuminate\Pagination\LengthAwarePaginator;
use Jhan\Kernel\Application\Mappers\BaseMapper;
use Jhan\Paginator\Domain\Dto\PaginatorDto;

class PaginatorDtoMapper extends BaseMapper
{
    protected function getNewDto(): PaginatorDto
    {
        return new PaginatorDto();
    }

    public function createFromPaginate(LengthAwarePaginator $records):PaginatorDto
    {
        $dto = $this->getNewDto();
        $dto->path = $records->path();
        $dto->items = $records->items();
        $dto->totalItems = $records->total();
        $dto->totalPages = $records->lastPage();
        $dto->perPage = $records->perPage();
        $dto->currentPage = request()->query('page', 1);
        $dto->query = request()->query();
        $dto->startPage = max(1, $dto->currentPage - 5);
        $dto->endPage = min($dto->startPage + 9, $dto->totalPages);
        return $dto;
    }

}
