<?php

namespace Jhan\Kernel\Domain\Exceptions;

class BaseException extends \Exception
{
    protected $message = 'Server error';

    protected $code = 500;

    protected $errors = [];

    public function __construct(?string $message = null, int $code = 0, array $errors = [], ?Throwable $previous = null)
    {
        parent::__construct();
        $this->message = $message ?? $this->message;
        $this->errors = $errors ?? $this->errors;
    }

    public function getErrors():array
    {
        return $this->errors;
    }
}
