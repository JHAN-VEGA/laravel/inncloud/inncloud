<?php

namespace Jhan\Kernel\Infrastructure\Interfaces\Repositories;

use App\Models\User;

interface BaseRepositoryInterface
{
    public function setUser(User $user):self;
}
