<?php

namespace Jhan\Kernel\Infrastructure\Repositories;

use App\Models\User;
use Jhan\Kernel\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

abstract class BaseRepository implements BaseRepositoryInterface
{
    protected ?User $user = null;

    abstract public function getTableName():string;

    abstract public function getDatabaseConnection():string;

    public function setUser(User $user): BaseRepositoryInterface
    {
        $this->user = $user;
        return $this;
    }

}
