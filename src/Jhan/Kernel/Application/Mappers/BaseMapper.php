<?php

namespace Jhan\Kernel\Application\Mappers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Jhan\Kernel\Domain\Dto\BaseDto;

abstract class BaseMapper
{
    abstract protected function getNewDto(): BaseDto;

    public function createFromDbRecord(object $dbRecord):BaseDto
    {
        $dto = $this->getNewDto();
        foreach ($dbRecord as $key => $value) {
            $dto->{$key} = $value;
        }
        return $dto;
    }

    public function createFromDbRecords(Collection $dbRecords):array
    {
        $dtos = [];
        foreach ($dbRecords as $dbRecord) {
            $dtos[] = $this->createFromDbRecord($dbRecord);
        }
        return $dtos;
    }

    public function createFromRequest(Request $request)
    {
        $dto = $this->getNewDto();
        foreach ($request->all() as $key => $value) {
            $dto->{$key} = $value;
        }
        return $dto;
    }
}
