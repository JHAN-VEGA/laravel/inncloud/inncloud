<?php

namespace Jhan\Http\Infrastructure\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Jhan\Kernel\Domain\Exceptions\BaseException;

abstract class BaseController extends Controller
{
    public function execWithJsonSuccessResponse($callback)
    {
        try{
            DB::beginTransaction();
            $response = $callback();
            $response = array_merge([
                'success' => true,
                'code' => 200,
                'message' => ''
            ], $response);
            DB::commit();
        } catch (ValidationException $exception) {
            DB::rollBack();
            $errors = [];
            foreach ($exception->errors() as $field => $error) {
                $errors[$field] = $error[0];
            }
            $response = response()->json([
                'success' => false,
                'code' => 422,
                'message' => $exception->getMessage(),
                'errors' => $errors
            ],422);
        }catch (BaseException $exception) {
            DB::rollBack();
            report($exception);
            $code = (int)$exception->getCode() < 100 || $exception->getCode() > 599 ? 500 : $exception->getCode();
            $errors = [];
            foreach ($exception->getErrors() as $field => $error) {
                $errors[$field] = $error;
            }
            $response = response()->json([
                'success' => false,
                'code' => $code,
                'message' => $exception->getMessage(),
                'errors' => $errors
            ], $code);
        }
        return $response;
    }

    public function execWithHttpResponse($callback)
    {
        try{
            DB::beginTransaction();
            $response = $callback();
            DB::commit();
        }catch (\Exception $exception) {
            DB::rollBack();
            report($exception);
            return back()->withErrors($exception->getMessage());
        }catch (\Throwable $exception) {
            DB::rollBack();
            report($exception);
            return back()->withErrors($exception->getMessage());
        }
        return $response;
    }

}
