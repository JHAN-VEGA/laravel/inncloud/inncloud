<?php

namespace DocumentManager\Domain\Dto;

use Jhan\Kernel\Domain\Dto\BaseDto;

class DocumentDto extends BaseDto
{
    public int $id;
    public string $name;
    public string $code;
    public string $body;
    public int $documentTypeId;
    public string $documentTypeName;
    public string $documentTypeCode;
    public int $processId;
    public string $processName;
    public string $processCode;
}
