<?php

namespace DocumentManager\Domain\Dto;

use Jhan\Kernel\Domain\Dto\BaseDto;

class DocumentPaginatorDto extends BaseDto
{
    public int $id;
    public string $name;
    public string $code;
    public int $processId;
    public string $processName;
    public string $processCode;
    public int $documentTypeId;
    public string $documentTypeName;
    public string $documentTypeCode;
}
