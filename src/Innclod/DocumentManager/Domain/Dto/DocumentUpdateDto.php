<?php

namespace DocumentManager\Domain\Dto;

use Jhan\Kernel\Domain\Dto\BaseDto;

class DocumentUpdateDto extends BaseDto
{
    public int $id;
    public string $name;
    public string $body;
}
