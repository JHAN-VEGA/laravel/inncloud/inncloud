<?php

namespace DocumentManager\Domain\Dto;

use Jhan\Kernel\Domain\Dto\BaseDto;

class DocumentNewDto extends BaseDto
{
    public string $name;
    public string $code;
    public string $body;
    public int $documentTypeId;
    public int $processId;
}
