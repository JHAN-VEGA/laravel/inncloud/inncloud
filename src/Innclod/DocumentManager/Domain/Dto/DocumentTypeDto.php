<?php

namespace DocumentManager\Domain\Dto;

use Jhan\Kernel\Domain\Dto\BaseDto;

class DocumentTypeDto extends BaseDto
{
    public int $id;
    public string $name;
    public string $code;
}
