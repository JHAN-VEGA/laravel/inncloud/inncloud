<?php

namespace DocumentManager\Domain\Dto;

use Jhan\Kernel\Domain\Dto\BaseDto;

class ProcessDto extends BaseDto
{
    public int $id;

    public string $name;

    public string $code;
}
