<?php

namespace DocumentManager\Domain\Exceptions;

use Jhan\Kernel\Domain\Exceptions\BaseException;

class DocumentNotFoundException extends BaseException
{
    protected $message = 'Documento no encontrado en el sistema';
    protected $code = 404;
}
