<?php

namespace DocumentManager\Domain\Exceptions;

use Jhan\Kernel\Domain\Exceptions\BaseException;

class DocumentTypeNotFoundException extends BaseException
{
    protected $message = 'Tipo de documento no encontrado en el sistema.';

    protected $code = 404;
}
