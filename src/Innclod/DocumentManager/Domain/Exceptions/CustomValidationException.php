<?php

namespace DocumentManager\Domain\Exceptions;

use Jhan\Kernel\Domain\Exceptions\BaseException;

class CustomValidationException extends BaseException
{
    protected $message = 'Registros no encontrados en el sistema.';
    protected $code = 404;
    protected $errors = [];
}
