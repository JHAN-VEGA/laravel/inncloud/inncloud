<?php

namespace DocumentManager\Domain\Exceptions;

use Jhan\Kernel\Domain\Exceptions\BaseException;

class ProcessNotFoundException extends BaseException
{
    protected $message = 'Proeso no encontrado en el sistema';
    protected $code = 404;
}
