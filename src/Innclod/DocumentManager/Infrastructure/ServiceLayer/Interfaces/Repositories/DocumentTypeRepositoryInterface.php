<?php

namespace DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories;

use DocumentManager\Domain\Dto\DocumentTypeDto;
use Jhan\Kernel\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

interface DocumentTypeRepositoryInterface extends BaseRepositoryInterface
{
    public function getById(int $id):? DocumentTypeDto;

    public function existById(int $id):bool;
}
