<?php

namespace DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories;

use DocumentManager\Domain\Dto\DocumentDto;
use DocumentManager\Domain\Dto\DocumentNewDto;
use DocumentManager\Domain\Dto\DocumentUpdateDto;
use Jhan\Kernel\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

interface DocumentRepositoryInterface extends BaseRepositoryInterface
{
    public function store(DocumentNewDto $dto):int;
    public function getById(int $id): ?DocumentDto;
    public function update(DocumentUpdateDto $dto):self;
    public function delete(int $id):self;
    public function getCountByProcessIdAndDocumentTypeId(int $processId, int $documentTypeId):int;
}
