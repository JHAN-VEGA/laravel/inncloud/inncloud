<?php

namespace DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories;

use DocumentManager\Domain\Dto\ProcessDto;
use Jhan\Kernel\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

interface ProcessRepositoryInterface extends BaseRepositoryInterface
{
    public function getById(int $id):? ProcessDto;

    public function existById(int $id):bool;
}
