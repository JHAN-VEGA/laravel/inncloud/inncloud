<?php

namespace DocumentManager\Infrastructure\ServiceLayer\Repositories;

use DocumentManager\Application\Mappers\ProcessDtoMapper;
use DocumentManager\Domain\Dto\ProcessDto;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Jhan\Kernel\Infrastructure\Repositories\BaseRepository;

class ProcessRepository extends BaseRepository implements ProcessRepositoryInterface
{
    public function getTableName(): string
    {
        return 'PRO_PROCESO';
    }

    public function getDatabaseConnection(): string
    {
        return 'pgsql';
    }

    public function getById(int $id):? ProcessDto
    {
        $record = DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->select([
                'PRO_ID AS id',
                'PRO_NOMBRE as name',
                'PRO_PREFIJO as code'
            ])
            ->where('PRO_ID', '=', $id)
            ->first();

        if (is_null($record)) return null;

        return (new ProcessDtoMapper())
            ->createFromDbRecord($record);
    }

    public function existById(int $id): bool
    {
        return DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->where('PRO_ID', '=', $id)
            ->exists();
    }


}
