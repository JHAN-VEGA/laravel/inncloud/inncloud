<?php

namespace DocumentManager\Infrastructure\ServiceLayer\Repositories;

use DocumentManager\Application\Mappers\DocumentTypeDtoMapper;
use DocumentManager\Domain\Dto\DocumentTypeDto;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Jhan\Kernel\Infrastructure\Repositories\BaseRepository;

class DocumentTypeRepository extends BaseRepository implements DocumentTypeRepositoryInterface
{
    public function getTableName(): string
    {
        return 'TIP_TIPO_DOC';
    }

    public function getDatabaseConnection(): string
    {
        return 'pgsql';
    }

    public function getById(int $id):? DocumentTypeDto
    {
        $record = DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->select([
                'TIP_ID AS id',
                'TIP_NOMBRE as name',
                'TIP_PREFIJO as code'
            ])
            ->where('TIP_ID', '=', $id)
            ->first();

        if (is_null($record)) return null;

        return (new DocumentTypeDtoMapper())
            ->createFromDbRecord($record);
    }

    public function existById(int $id): bool
    {
        return DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->where('TIP_ID', '=', $id)
            ->exists();
    }

}
