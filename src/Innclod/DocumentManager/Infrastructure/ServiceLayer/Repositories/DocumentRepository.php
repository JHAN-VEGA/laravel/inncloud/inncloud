<?php

namespace DocumentManager\Infrastructure\ServiceLayer\Repositories;

use DocumentManager\Application\Mappers\DocumentDtoMapper;
use DocumentManager\Domain\Dto\DocumentDto;
use DocumentManager\Domain\Dto\DocumentNewDto;
use DocumentManager\Domain\Dto\DocumentUpdateDto;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Jhan\Kernel\Infrastructure\Repositories\BaseRepository;

class DocumentRepository extends BaseRepository implements DocumentRepositoryInterface
{
    public function getTableName(): string
    {
        return 'DOC_DOCUMENTO';
    }

    public function getDatabaseConnection(): string
    {
        return 'pgsql';
    }

    public function getById(int $id): ?DocumentDto
    {
        $record = DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->select([
                'DOC_ID AS id',
                'DOC_NOMBRE AS name',
                'DOC_CODIGO AS code',
                'DOC_CONTENIDO AS body',
                'PRO_PROCESO.PRO_ID AS process_id',
                'PRO_PROCESO.PRO_NOMBRE AS process_name',
                'PRO_PROCESO.PRO_PREFIJO AS process_code',
                'TIP_TIPO_DOC.TIP_ID AS document_type_id',
                'TIP_TIPO_DOC.TIP_NOMBRE AS document_type_name',
                'TIP_TIPO_DOC.TIP_PREFIJO AS document_type_code',
            ])
            ->leftJoin('PRO_PROCESO', 'PRO_PROCESO.PRO_ID', '=', 'DOC_DOCUMENTO.DOC_ID_PROCESO')
            ->leftJoin('TIP_TIPO_DOC', 'TIP_TIPO_DOC.TIP_ID', '=', 'DOC_DOCUMENTO.DOC_ID_TIPO')
            ->where('DOC_ID', '=', $id)
            ->first();

        if (is_null($record)) return null;

        return (new DocumentDtoMapper())
            ->createFromDbRecord($record);
    }

    public function store(DocumentNewDto $dto): int
    {
        return DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->insertGetId([
                'DOC_NOMBRE' => $dto->name,
                'DOC_CODIGO' => $dto->code,
                'DOC_CONTENIDO' => $dto->body,
                'DOC_ID_TIPO' => $dto->documentTypeId,
                'DOC_ID_PROCESO' => $dto->processId,
                'user_who_created_id' => $this->user->id ?? null,
                'created_at'=> 'now()'
            ],'DOC_ID');
    }

    public function update(DocumentUpdateDto $dto): self
    {
        DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->where('DOC_ID', '=', $dto->id)
            ->update([
                'DOC_NOMBRE' => $dto->name,
                'DOC_CONTENIDO' => $dto->body,
                'user_who_updated_id' => $this->user->id ?? null,
                'updated_at'=> 'now()'
            ]);

        return $this;
    }

    public function delete(int $id): self
    {
        DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->where('DOC_ID', '=', $id)
            ->delete();
        return $this;
    }

    public function getCountByProcessIdAndDocumentTypeId(int $processId, int $documentTypeId): int
    {
        return DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->where('DOC_ID_PROCESO', '=', $processId)
            ->where('DOC_ID_TIPO', '=', $documentTypeId)
            ->count();
    }

}
