<?php

namespace DocumentManager\Infrastructure\ServiceLayer;

use DocumentManager\Application\Interfaces\Paginators\DocumentPaginatorInterface;
use DocumentManager\Application\Interfaces\Services\DocumentManagerServiceInterface;
use DocumentManager\Application\Interfaces\Services\DocumentManagerValidateServiceInterface;
use DocumentManager\Application\Interfaces\Services\DocumentTypeServiceInterface;
use DocumentManager\Application\Interfaces\Services\ProcessServiceInterface;
use DocumentManager\Application\Paginators\DocumentPaginator;
use DocumentManager\Application\Services\DocumentManagerService;
use DocumentManager\Application\Services\DocumentManagerValidateService;
use DocumentManager\Application\Services\DocumentTypeService;
use DocumentManager\Application\Services\ProcessService;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentRepositoryInterface;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use DocumentManager\Infrastructure\ServiceLayer\Repositories\DocumentRepository;
use DocumentManager\Infrastructure\ServiceLayer\Repositories\ProcessRepository;
use DocumentManager\Infrastructure\ServiceLayer\Repositories\DocumentTypeRepository;
use Illuminate\Support\ServiceProvider;

class DocumentManagerServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->registerRoutes();
        $this->registerBinds();
        $this->registerMiddlewares();
    }

    public function registerRoutes():void
    {
        $this->loadRoutesFrom(__DIR__.'/../Routes/web.php');
    }

    public function registerBinds():void
    {
        $this->app->bind(DocumentPaginatorInterface::class, DocumentPaginator::class);
        $this->app->bind(DocumentRepositoryInterface::class, DocumentRepository::class);
        $this->app->bind(DocumentTypeRepositoryInterface::class, DocumentTypeRepository::class);
        $this->app->bind(ProcessRepositoryInterface::class, ProcessRepository::class);
        $this->app->bind(DocumentManagerServiceInterface::class, DocumentManagerService::class);
        $this->app->bind(DocumentManagerValidateServiceInterface::class, DocumentManagerValidateService::class);
        $this->app->bind(DocumentTypeServiceInterface::class, DocumentTypeService::class);
        $this->app->bind(ProcessServiceInterface::class, ProcessService::class);
    }

    public function registerMiddlewares():void
    {
        $router = $this->app['router'];
    }

}
