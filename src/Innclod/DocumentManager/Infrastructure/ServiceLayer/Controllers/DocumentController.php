<?php

namespace DocumentManager\Infrastructure\ServiceLayer\Controllers;

use DocumentManager\Application\Interfaces\Paginators\DocumentPaginatorInterface;
use DocumentManager\Application\Interfaces\Services\DocumentManagerServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Jhan\Http\Infrastructure\Controllers\BaseController;

class DocumentController extends BaseController
{
    private DocumentManagerServiceInterface $documentManagerService;

    private DocumentPaginatorInterface $documentPaginator;

    public function __construct()
    {
        $this->documentManagerService = App::make(DocumentManagerServiceInterface::class);
        $this->documentPaginator = App::make(DocumentPaginatorInterface::class);
    }

    public function index(Request $request)
    {
        return $this->execWithHttpResponse(function () use ($request) {

            $this->documentPaginator->initialize();

            match ($request->get('searchBy')) {
                'name' => $this->documentPaginator->filterByName($request->get('value')),
                'code' => $this->documentPaginator->filterByCode($request->get('value')),
                default => null,
            };

            return view('Innclod.DocumentManager.index', [
                'data' => $this->documentPaginator->paginate(recordsByPage: 10)
            ]);
        });
    }

    public function create()
    {
        return view('Innclod.DocumentManager.create');
    }

    public function edit(int $id)
    {
        return view('Innclod.DocumentManager.edit', [
            'data' => $this->documentManagerService->getById($id)
        ]);
    }

    public function store(Request $request)
    {
        return $this->execWithJsonSuccessResponse(function () use ($request){

            $id = $this->documentManagerService
                ->store(
                    (new DocumentValidateRulesController())->validateStoreRequest($request)
                );

            return [
                'message' => 'Documento creado con éxito.',
                'id' => $id
            ];
        });
    }

    public function update(Request $request, $id)
    {
        return $this->execWithJsonSuccessResponse(function () use ($request, $id){

            $this->documentManagerService
                ->update(
                    (new DocumentValidateRulesController())->validateUpdateRequest($request, $id)
                );

            return [
                'message' => 'Documento actualizado con éxito.',
            ];
        });
    }

    public function delete(int $id)
    {
        return $this->execWithJsonSuccessResponse(function () use ($id){

            $this->documentManagerService
                ->delete($id);

            return [
                'message' => 'Documento eliminado con éxito.',
            ];
        });
    }
}
