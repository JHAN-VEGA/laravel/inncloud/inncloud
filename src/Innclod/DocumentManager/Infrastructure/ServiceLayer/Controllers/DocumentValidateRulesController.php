<?php

namespace DocumentManager\Infrastructure\ServiceLayer\Controllers;

use DocumentManager\Application\Mappers\DocumentNewDtoMapper;
use DocumentManager\Application\Mappers\DocumentUpdateDtoMapper;
use DocumentManager\Domain\Dto\DocumentNewDto;
use DocumentManager\Domain\Dto\DocumentUpdateDto;
use Illuminate\Http\Request;

class DocumentValidateRulesController
{
    public function validateStoreRequest(Request $request):DocumentNewDto
    {
        $request->validate([
            'name' => ['required', 'string'],
            'body' => ['required', 'string'],
            'document_type_id' => ['required', 'integer'],
            'process_id' => ['required', 'integer'],
        ]);

        return (new DocumentNewDtoMapper())
            ->createFromRequest($request);
    }

    public function validateUpdateRequest(Request $request, int $id):DocumentUpdateDto
    {
        $request->validate([
            'name' => ['required', 'string'],
            'body' => ['required', 'string'],
        ]);

        $dto = (new DocumentUpdateDtoMapper())
            ->createFromRequest($request);

        $dto->id = $id;

        return $dto;
    }
}
