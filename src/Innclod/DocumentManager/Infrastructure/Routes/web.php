<?php

use DocumentManager\Infrastructure\ServiceLayer\Controllers\DocumentController;
use Illuminate\Support\Facades\Route;

Route::prefix('documents')->middleware(['web', 'auth'])->group(function () {

    Route::get('', [DocumentController::class, 'index'])
        ->name('documents.index');

    Route::get('create', [DocumentController::class, 'create'])
        ->name('documents.create');

    Route::post('store', [DocumentController::class, 'store'])
        ->name('documents.store');

    Route::get('{id}/edit', [DocumentController::class, 'edit'])
        ->name('documents.edit');

    Route::post('{id}/update', [DocumentController::class, 'update'])
        ->name('documents.update');

    Route::post('{id}/delete', [DocumentController::class, 'delete'])
        ->name('documents.delete');

});
