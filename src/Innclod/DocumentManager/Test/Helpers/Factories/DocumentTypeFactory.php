<?php

namespace DocumentManager\Test\Helpers\Factories;

use Illuminate\Support\Str;

class DocumentTypeFactory extends BaseFactory
{
    public function getTableName(): string
    {
        return 'TIP_TIPO_DOC';
    }

    public function getDatabaseConnection(): string
    {
        return 'pgsql';
    }

    protected function getAttributes(): array
    {
        return [
            'TIP_NOMBRE' => Str::random(10),
            'TIP_PREFIJO' => Str::random(5),
        ];
    }

}
