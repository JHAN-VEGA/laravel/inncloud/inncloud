<?php

namespace DocumentManager\Test\Helpers\Factories;

use Illuminate\Support\Str;

class DocumentFactory extends BaseFactory
{
    public function getTableName(): string
    {
        return 'DOC_DOCUMENTO';
    }

    public function getDatabaseConnection(): string
    {
        return 'pgsql';
    }

    protected function getAttributes(): array
    {
        return [
            'DOC_NOMBRE' => Str::random(10),
            'DOC_CODIGO' => Str::random(10),
            'DOC_CONTENIDO' => Str::random(50),
            'DOC_ID_TIPO' => 1,
            'DOC_ID_PROCESO' => 1,
        ];
    }

}
