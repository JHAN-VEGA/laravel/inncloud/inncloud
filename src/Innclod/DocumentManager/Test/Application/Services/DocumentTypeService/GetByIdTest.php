<?php

namespace DocumentManager\Test\Application\Services\DocumentTypeService;

use DocumentManager\Application\Interfaces\Services\DocumentTypeServiceInterface;
use DocumentManager\Domain\Dto\DocumentTypeDto;
use DocumentManager\Domain\Exceptions\DocumentTypeNotFoundException;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use DocumentManager\Test\BaseTest;
use Illuminate\Support\Facades\App;

class GetByIdTest extends BaseTest
{
    public function testIsGetByIdWorking()
    {
        $documentTypeRepoMock = \Mockery::mock(DocumentTypeRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentTypeRepoMock->shouldReceive('getById')
            ->once()
            ->andReturnUsing(fn () => new DocumentTypeDto());

        $this->instance(DocumentTypeRepositoryInterface::class, $documentTypeRepoMock);

        $response = (App::make(DocumentTypeServiceInterface::class))
            ->getById(1);

        $this->assertInstanceOf(DocumentTypeDto::class, $response);
    }

    public function testIsGetByIdFailing()
    {
        $documentTypeRepoMock = \Mockery::mock(DocumentTypeRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentTypeRepoMock->shouldReceive('getById')
            ->once()
            ->andReturnNull();

        $this->instance(DocumentTypeRepositoryInterface::class, $documentTypeRepoMock);

        try {
            (App::make(DocumentTypeServiceInterface::class))
                ->getById(1);
        }catch(DocumentTypeNotFoundException $exception) {
            $this->assertEquals(404, $exception->getCode());
        }

    }
}
