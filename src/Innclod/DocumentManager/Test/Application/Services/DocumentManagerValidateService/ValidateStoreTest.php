<?php

namespace DocumentManager\Test\Application\Services\DocumentManagerValidateService;

use DocumentManager\Application\Interfaces\Services\DocumentManagerValidateServiceInterface;
use DocumentManager\Domain\Dto\DocumentNewDto;
use DocumentManager\Domain\Exceptions\CustomValidationException;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;
use DocumentManager\Test\BaseTest;
use Illuminate\Support\Facades\App;

class ValidateStoreTest extends BaseTest
{
    public function testIsValidateStoreWorking()
    {
        $documentTypeRepoMock = \Mockery::mock(DocumentTypeRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentTypeRepoMock->shouldReceive('existById')
            ->once()
            ->andReturnTrue();

        $processRepoMock = \Mockery::mock(ProcessRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $processRepoMock->shouldReceive('existById')
            ->once()
            ->andReturnTrue();

        $this->instance(DocumentTypeRepositoryInterface::class, $documentTypeRepoMock);
        $this->instance(ProcessRepositoryInterface::class, $processRepoMock);

        $dto = new DocumentNewDto();
        $dto->documentTypeId = 1;
        $dto->processId = 1;

        (App::make(DocumentManagerValidateServiceInterface::class))
            ->validateStore($dto);

        $this->assertTrue(true);
    }

    public function testIsValidateStoreFailing()
    {
        $documentTypeRepoMock = \Mockery::mock(DocumentTypeRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentTypeRepoMock->shouldReceive('existById')
            ->once()
            ->andReturnFalse();

        $processRepoMock = \Mockery::mock(ProcessRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $processRepoMock->shouldReceive('existById')
            ->once()
            ->andReturnFalse();

        $this->instance(DocumentTypeRepositoryInterface::class, $documentTypeRepoMock);
        $this->instance(ProcessRepositoryInterface::class, $processRepoMock);

        $dto = new DocumentNewDto();
        $dto->documentTypeId = 1;
        $dto->processId = 1;

        try {
            (App::make(DocumentManagerValidateServiceInterface::class))
                ->validateStore($dto);
        }catch (CustomValidationException $exception) {
            $this->assertEquals(404, $exception->getCode());
            $this->assertCount(2, $exception->getErrors());
        }
    }
}
