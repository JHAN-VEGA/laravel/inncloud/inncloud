<?php

namespace DocumentManager\Test\Application\Services\DocumentManagerService;

use DocumentManager\Application\Interfaces\Services\DocumentManagerServiceInterface;
use DocumentManager\Domain\Dto\DocumentDto;
use DocumentManager\Domain\Dto\DocumentUpdateDto;
use DocumentManager\Domain\Exceptions\DocumentNotFoundException;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentRepositoryInterface;
use DocumentManager\Test\BaseTest;
use Illuminate\Support\Facades\App;

class UpdateTest extends BaseTest
{
    public function testIsUpdateWorking()
    {
        $documentRepoMock = \Mockery::mock(DocumentRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentRepoMock->shouldReceive('getById')
            ->once()
            ->andReturnUsing(fn () => new DocumentDto());

        $documentRepoMock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $documentRepoMock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        $this->actingAs($this->user);
        $this->instance(DocumentRepositoryInterface::class, $documentRepoMock);

        $dto = new DocumentUpdateDto();
        $dto->id = 1;

        (App::make(DocumentManagerServiceInterface::class))
            ->update($dto);

        $this->assertTrue(true);
    }
}
