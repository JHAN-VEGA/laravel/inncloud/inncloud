<?php

namespace DocumentManager\Test\Application\Services\DocumentManagerService;

use DocumentManager\Application\Interfaces\Services\DocumentManagerServiceInterface;
use DocumentManager\Application\Interfaces\Services\DocumentManagerValidateServiceInterface;
use DocumentManager\Application\Interfaces\Services\DocumentTypeServiceInterface;
use DocumentManager\Application\Interfaces\Services\ProcessServiceInterface;
use DocumentManager\Domain\Dto\DocumentDto;
use DocumentManager\Domain\Dto\DocumentNewDto;
use DocumentManager\Domain\Dto\DocumentTypeDto;
use DocumentManager\Domain\Dto\DocumentUpdateDto;
use DocumentManager\Domain\Dto\ProcessDto;
use DocumentManager\Domain\Exceptions\DocumentNotFoundException;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentRepositoryInterface;
use DocumentManager\Test\BaseTest;
use Illuminate\Support\Facades\App;

class StoreTest extends BaseTest
{
    public function testIsStoreWorking()
    {
        $documentManagerValidateServiceMock = \Mockery::mock(DocumentManagerValidateServiceInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentManagerValidateServiceMock->shouldReceive('validateStore')
            ->once()
            ->andReturn();

        $processServiceMock = \Mockery::mock(ProcessServiceInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $processServiceMock->shouldReceive('getById')
            ->once()
            ->andReturnUsing(function () {
                $dto = new ProcessDto();
                $dto->code = 'Código';
                $dto->id = 1;
                return $dto;
            });

        $documentTypeServiceMock = \Mockery::mock(DocumentTypeServiceInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentTypeServiceMock->shouldReceive('getById')
            ->once()
            ->andReturnUsing(function () {
                $dto = new DocumentTypeDto();
                $dto->code = 'Código';
                $dto->id = 1;
                return $dto;
            });

        $documentRepoMock = \Mockery::mock(DocumentRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentRepoMock->shouldReceive('getCountByProcessIdAndDocumentTypeId')
            ->once()
            ->andReturn(1);

        $documentRepoMock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $documentRepoMock->shouldReceive('store')
            ->once()
            ->andReturn(5);

        $this->actingAs($this->user);
        $this->instance(DocumentManagerValidateServiceInterface::class, $documentManagerValidateServiceMock);
        $this->instance(ProcessServiceInterface::class, $processServiceMock);
        $this->instance(DocumentTypeServiceInterface::class, $documentTypeServiceMock);
        $this->instance(DocumentRepositoryInterface::class, $documentRepoMock);

        $dto = new DocumentNewDto();
        $dto->documentTypeId = 1;
        $dto->processId = 1;

        $response = (App::make(DocumentManagerServiceInterface::class))
            ->store($dto);

        $this->assertIsInt($response);
    }
}
