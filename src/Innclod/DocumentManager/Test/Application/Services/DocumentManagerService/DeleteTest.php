<?php

namespace DocumentManager\Test\Application\Services\DocumentManagerService;

use DocumentManager\Application\Interfaces\Services\DocumentManagerServiceInterface;
use DocumentManager\Domain\Dto\DocumentDto;
use DocumentManager\Domain\Exceptions\DocumentNotFoundException;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentRepositoryInterface;
use DocumentManager\Test\BaseTest;
use Illuminate\Support\Facades\App;

class DeleteTest extends BaseTest
{
    public function testIsDeleteWorking()
    {
        $documentRepoMock = \Mockery::mock(DocumentRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentRepoMock->shouldReceive('getById')
            ->once()
            ->andReturnUsing(fn () => new DocumentDto());

        $documentRepoMock->shouldReceive('setUser')
            ->once()
            ->andReturnSelf();

        $documentRepoMock->shouldReceive('delete')
            ->once()
            ->andReturnSelf();

        $this->actingAs($this->user);
        $this->instance(DocumentRepositoryInterface::class, $documentRepoMock);

        (App::make(DocumentManagerServiceInterface::class))
            ->delete(1);

        $this->assertTrue(true);
    }
}
