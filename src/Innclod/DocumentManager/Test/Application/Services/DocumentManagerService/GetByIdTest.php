<?php

namespace DocumentManager\Test\Application\Services\DocumentManagerService;

use DocumentManager\Application\Interfaces\Services\DocumentManagerServiceInterface;
use DocumentManager\Domain\Dto\DocumentDto;
use DocumentManager\Domain\Exceptions\DocumentNotFoundException;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentRepositoryInterface;
use DocumentManager\Test\BaseTest;
use Illuminate\Support\Facades\App;

class GetByIdTest extends BaseTest
{
    public function testIsGetByIdWorking()
    {
        $documentRepoMock = \Mockery::mock(DocumentRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentRepoMock->shouldReceive('getById')
            ->once()
            ->andReturnUsing(fn () => new DocumentDto());

        $this->instance(DocumentRepositoryInterface::class, $documentRepoMock);

        $response = (App::make(DocumentManagerServiceInterface::class))
            ->getById(1);

        $this->assertInstanceOf(DocumentDto::class, $response);
    }

    public function testIsGetByIdFailing()
    {
        $documentRepoMock = \Mockery::mock(DocumentRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentRepoMock->shouldReceive('getById')
            ->once()
            ->andReturnNull();

        $this->instance(DocumentRepositoryInterface::class, $documentRepoMock);

        try {
            (App::make(DocumentManagerServiceInterface::class))
                ->getById(1);
        }catch(DocumentNotFoundException $exception) {
            $this->assertEquals(404, $exception->getCode());
        }

    }
}
