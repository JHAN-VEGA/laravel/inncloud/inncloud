<?php

namespace DocumentManager\Test\Application\Services\ProcessService;

use DocumentManager\Application\Interfaces\Services\ProcessServiceInterface;
use DocumentManager\Domain\Dto\ProcessDto;
use DocumentManager\Domain\Exceptions\ProcessNotFoundException;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;
use DocumentManager\Test\BaseTest;
use Illuminate\Support\Facades\App;

class GetByIdTest extends BaseTest
{
    public function testIsGetByIdWorking()
    {
        $processRepoMock = \Mockery::mock(ProcessRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $processRepoMock->shouldReceive('getById')
            ->once()
            ->andReturnUsing(fn () => new ProcessDto());

        $this->instance(ProcessRepositoryInterface::class, $processRepoMock);

        $response = (App::make(ProcessServiceInterface::class))
            ->getById(1);

        $this->assertInstanceOf(ProcessDto::class, $response);
    }

    public function testIsGetByIdFailing()
    {
        $processRepoMock = \Mockery::mock(ProcessRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $processRepoMock->shouldReceive('getById')
            ->once()
            ->andReturnNull();

        $this->instance(ProcessRepositoryInterface::class, $processRepoMock);

        try {
            (App::make(ProcessServiceInterface::class))
                ->getById(1);
        }catch(ProcessNotFoundException $exception) {
            $this->assertEquals(404, $exception->getCode());
        }

    }
}
