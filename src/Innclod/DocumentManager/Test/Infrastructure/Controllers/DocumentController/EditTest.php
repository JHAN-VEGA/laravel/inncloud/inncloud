<?php

namespace DocumentManager\Test\Infrastructure\Controllers\DocumentController;

use DocumentManager\Application\Interfaces\Services\DocumentManagerServiceInterface;
use DocumentManager\Domain\Dto\DocumentDto;
use DocumentManager\Test\BaseTest;

class EditTest extends BaseTest
{
    public function testIsEditWorking()
    {
        $documentManagerServiceMock = \Mockery::mock(DocumentManagerServiceInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentManagerServiceMock->shouldReceive('getById')
            ->once()
            ->andReturnUsing(function () {
                $dto = new DocumentDto();
                $dto->id = 1;
                $dto->name = 'Documento';
                $dto->body = 'Contenido';
                $dto->code = 'Código';
                $dto->documentTypeId = 1;
                $dto->documentTypeName = 'Tipo de documento';
                $dto->documentTypeCode = 'Código tipo de documento';
                $dto->processId = 1;
                $dto->processName = 'Proceso';
                $dto->processCode = 'Código proceso';
                return $dto;
            });

        $this->instance(DocumentManagerServiceInterface::class, $documentManagerServiceMock);
        $this->actingAs($this->user);

        $response = $this->get(route('documents.edit', 1));
        $response->assertStatus(200);
        $response->assertViewIs('Innclod.DocumentManager.edit');
    }
}
