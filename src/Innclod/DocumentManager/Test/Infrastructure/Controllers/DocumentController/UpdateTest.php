<?php

namespace DocumentManager\Test\Infrastructure\Controllers\DocumentController;

use DocumentManager\Application\Interfaces\Services\DocumentManagerServiceInterface;
use DocumentManager\Domain\Dto\DocumentDto;
use DocumentManager\Test\BaseTest;

class UpdateTest extends BaseTest
{
    public function testIsUpdateWorking()
    {
        $documentManagerServiceMock = \Mockery::mock(DocumentManagerServiceInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentManagerServiceMock->shouldReceive('update')
            ->once()
            ->andReturnSelf();

        $this->instance(DocumentManagerServiceInterface::class, $documentManagerServiceMock);
        $this->actingAs($this->user);

        $response = $this->post(route('documents.update', 1), [
            'name' => 'Documento',
            'body' => 'Contenido',
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message']);
    }
}
