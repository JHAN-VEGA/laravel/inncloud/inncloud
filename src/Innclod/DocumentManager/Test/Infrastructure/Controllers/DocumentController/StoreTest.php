<?php

namespace DocumentManager\Test\Infrastructure\Controllers\DocumentController;

use DocumentManager\Application\Interfaces\Services\DocumentManagerServiceInterface;
use DocumentManager\Test\BaseTest;

class StoreTest extends BaseTest
{
    public function testIsStoreWorking()
    {
        $documentManagerServiceMock = \Mockery::mock(DocumentManagerServiceInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentManagerServiceMock->shouldReceive('store')
            ->once()
            ->andReturn(1);

        $this->instance(DocumentManagerServiceInterface::class, $documentManagerServiceMock);
        $this->actingAs($this->user);

        $response = $this->post(route('documents.store'), [
            'name' => 'Documento',
            'body' => 'contenido',
            'document_type_id' => 1,
            'process_id' => 1
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message', 'id']);
    }
}
