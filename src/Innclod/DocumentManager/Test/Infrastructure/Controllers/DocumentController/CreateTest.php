<?php

namespace DocumentManager\Test\Infrastructure\Controllers\DocumentController;

use DocumentManager\Test\BaseTest;

class CreateTest extends BaseTest
{
    public function testIsCreateWorking()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('documents.create'));
        $response->assertStatus(200);
        $response->assertViewIs('Innclod.DocumentManager.create');

    }
}
