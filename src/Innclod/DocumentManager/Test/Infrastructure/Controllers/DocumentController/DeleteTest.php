<?php

namespace DocumentManager\Test\Infrastructure\Controllers\DocumentController;

use DocumentManager\Application\Interfaces\Services\DocumentManagerServiceInterface;
use DocumentManager\Domain\Dto\DocumentDto;
use DocumentManager\Test\BaseTest;

class DeleteTest extends BaseTest
{
    public function testIsDeleteWorking()
    {
        $documentManagerServiceMock = \Mockery::mock(DocumentManagerServiceInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $documentManagerServiceMock->shouldReceive('delete')
            ->once()
            ->andReturnSelf();

        $this->instance(DocumentManagerServiceInterface::class, $documentManagerServiceMock);
        $this->actingAs($this->user);

        $response = $this->post(route('documents.delete', 1));

        $response->assertStatus(200);
        $response->assertJsonStructure(['success', 'code', 'message']);
    }
}
