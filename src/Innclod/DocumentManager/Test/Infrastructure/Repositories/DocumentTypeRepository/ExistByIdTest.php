<?php

namespace DocumentManager\Test\Infrastructure\Repositories\DocumentTypeRepository;

use DocumentManager\Domain\Dto\DocumentTypeDto;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use DocumentManager\Test\BaseTest;
use DocumentManager\Test\Helpers\Factories\DocumentTypeFactory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ExistByIdTest extends BaseTest
{
    use DatabaseTransactions;

    public function testIsExistByIdWorking()
    {
        $id = $this->getNewDocumentTypeId();
        $response = (App::make(DocumentTypeRepositoryInterface::class))
            ->existById($id);

        $this->assertTrue($response);
    }

    public function testIsExistByIdFailing()
    {
        $response = (App::make(DocumentTypeRepositoryInterface::class))
            ->existById(0);

        $this->assertFalse($response);
    }

    protected function getNewDocumentTypeId():int
    {
        $factory = new DocumentTypeFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get(), 'TIP_ID');
    }
}
