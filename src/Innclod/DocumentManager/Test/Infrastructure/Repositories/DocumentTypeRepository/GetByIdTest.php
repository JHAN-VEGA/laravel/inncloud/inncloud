<?php

namespace DocumentManager\Test\Infrastructure\Repositories\DocumentTypeRepository;

use DocumentManager\Domain\Dto\DocumentTypeDto;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use DocumentManager\Test\BaseTest;
use DocumentManager\Test\Helpers\Factories\DocumentTypeFactory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class GetByIdTest extends BaseTest
{
    use DatabaseTransactions;

    public function testIsGetByIdWorking()
    {
        $id = $this->getNewDocumentTypeId();
        $response = (App::make(DocumentTypeRepositoryInterface::class))
            ->getById($id);

        $this->assertInstanceOf(DocumentTypeDto::class, $response);
    }

    public function testIsGetByIdFailing()
    {
        $response = (App::make(DocumentTypeRepositoryInterface::class))
            ->getById(0);

        $this->assertNull($response);
    }

    protected function getNewDocumentTypeId():int
    {
        $factory = new DocumentTypeFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get(), 'TIP_ID');
    }
}
