<?php

namespace DocumentManager\Test\Infrastructure\Repositories\ProcessRepository;

use DocumentManager\Domain\Dto\DocumentTypeDto;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;
use DocumentManager\Test\BaseTest;
use DocumentManager\Test\Helpers\Factories\DocumentTypeFactory;
use DocumentManager\Test\Helpers\Factories\ProcessFactory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class ExistByIdTest extends BaseTest
{
    use DatabaseTransactions;

    public function testIsExistByIdWorking()
    {
        $id = $this->getNewDocumentTypeId();
        $response = (App::make(ProcessRepositoryInterface::class))
            ->existById($id);

        $this->assertTrue($response);
    }

    public function testIsExistByIdFailing()
    {
        $response = (App::make(ProcessRepositoryInterface::class))
            ->existById(0);

        $this->assertFalse($response);
    }

    protected function getNewDocumentTypeId():int
    {
        $factory = new ProcessFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get(), 'PRO_ID');
    }
}
