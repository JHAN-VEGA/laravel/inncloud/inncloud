<?php

namespace DocumentManager\Test\Infrastructure\Repositories\ProcessRepository;

use DocumentManager\Domain\Dto\ProcessDto;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;
use DocumentManager\Test\BaseTest;
use DocumentManager\Test\Helpers\Factories\ProcessFactory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class GetByIdTest extends BaseTest
{
    use DatabaseTransactions;

    public function testIsGetByIdWorking()
    {
        $id = $this->getNewProcessId();
        $response = (App::make(ProcessRepositoryInterface::class))
            ->getById($id);

        $this->assertInstanceOf(ProcessDto::class, $response);
    }

    public function testIsGetByIdFailing()
    {
        $response = (App::make(ProcessRepositoryInterface::class))
            ->getById(0);

        $this->assertNull($response);
    }

    protected function getNewProcessId():int
    {
        $factory = new ProcessFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get(), 'PRO_ID');
    }
}
