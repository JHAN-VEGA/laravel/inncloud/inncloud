<?php

namespace DocumentManager\Test\Infrastructure\Repositories\DocumentRepository;

use DocumentManager\Domain\Dto\DocumentNewDto;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentRepositoryInterface;
use DocumentManager\Test\BaseTest;
use DocumentManager\Test\Helpers\Factories\DocumentTypeFactory;
use DocumentManager\Test\Helpers\Factories\ProcessFactory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class StoreTest extends BaseTest
{
    use DatabaseTransactions;

    private DocumentRepositoryInterface $repo;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = App::make(DocumentRepositoryInterface::class);
    }

    public function testIsStoreWorking()
    {
        $dto = new DocumentNewDto();
        $dto->name = Str::random(10);
        $dto->code = Str::random(10);
        $dto->body = Str::random(20);
        $dto->documentTypeId = $this->getNewDocumentTypeId();
        $dto->processId = $this->getNewProcessId();

        $id = $this->repo->setUser($this->user)->store($dto);

        $this->assertDatabaseHas($this->repo->getTableName(), [
            'DOC_ID' => $id,
            'DOC_NOMBRE' => $dto->name,
            'DOC_CODIGO' => $dto->code,
            'DOC_CONTENIDO' => $dto->body,
            'DOC_ID_TIPO' => $dto->documentTypeId,
            'DOC_ID_PROCESO' => $dto->processId,
        ], $this->repo->getDatabaseConnection());
    }

    protected function getNewDocumentTypeId():int
    {
        $factory = new DocumentTypeFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get(), 'TIP_ID');
    }

    protected function getNewProcessId():int
    {
        $factory = new ProcessFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get(), 'PRO_ID');
    }

}
