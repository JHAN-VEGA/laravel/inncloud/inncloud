<?php

namespace DocumentManager\Test\Infrastructure\Repositories\DocumentRepository;

use DocumentManager\Domain\Dto\DocumentNewDto;
use DocumentManager\Domain\Dto\DocumentUpdateDto;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentRepositoryInterface;
use DocumentManager\Test\BaseTest;
use DocumentManager\Test\Helpers\Factories\DocumentFactory;
use DocumentManager\Test\Helpers\Factories\DocumentTypeFactory;
use DocumentManager\Test\Helpers\Factories\ProcessFactory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UpdateTest extends BaseTest
{
    use DatabaseTransactions;

    private DocumentRepositoryInterface $repo;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repo = App::make(DocumentRepositoryInterface::class);
    }

    public function testIsStoreWorking()
    {
        $dto = new DocumentUpdateDto();
        $dto->id = $this->getNewDocumentId();
        $dto->name = Str::random(10);
        $dto->body = Str::random(20);

        $this->repo->setUser($this->user)->update($dto);

        $this->assertDatabaseHas($this->repo->getTableName(), [
            'DOC_ID' => $dto->id,
            'DOC_NOMBRE' => $dto->name,
            'DOC_CONTENIDO' => $dto->body,
        ], $this->repo->getDatabaseConnection());
    }

    protected function getNewDocumentId():int
    {
        $factory = new DocumentFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get([
                'DOC_ID_TIPO' => $this->getNewDocumentTypeId(),
                'DOC_ID_PROCESO' => $this->getNewProcessId()
            ]), 'DOC_ID');
    }

    protected function getNewDocumentTypeId():int
    {
        $factory = new DocumentTypeFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get(), 'TIP_ID');
    }

    protected function getNewProcessId():int
    {
        $factory = new ProcessFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get(), 'PRO_ID');
    }

}
