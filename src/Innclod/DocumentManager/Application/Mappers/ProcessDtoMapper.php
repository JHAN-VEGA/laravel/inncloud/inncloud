<?php

namespace DocumentManager\Application\Mappers;

use DocumentManager\Domain\Dto\ProcessDto;
use Jhan\Kernel\Application\Mappers\BaseMapper;

class ProcessDtoMapper extends BaseMapper
{
    protected function getNewDto(): ProcessDto
    {
        return new ProcessDto();
    }

    public function createFromDbRecord(object $dbRecord):ProcessDto
    {
        $dto = $this->getNewDto();
        $dto->id = $dbRecord->id;
        $dto->name = $dbRecord->code;
        $dto->code = $dbRecord->code;
        return $dto;
    }

}
