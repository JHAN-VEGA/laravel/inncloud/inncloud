<?php

namespace DocumentManager\Application\Mappers;

use DocumentManager\Domain\Dto\DocumentTypeDto;
use Jhan\Kernel\Application\Mappers\BaseMapper;

class DocumentTypeDtoMapper extends BaseMapper
{
    protected function getNewDto(): DocumentTypeDto
    {
        return new DocumentTypeDto();
    }

    public function createFromDbRecord(object $dbRecord):DocumentTypeDto
    {
        $dto = $this->getNewDto();
        $dto->id = $dbRecord->id;
        $dto->name = $dbRecord->code;
        $dto->code = $dbRecord->code;
        return $dto;
    }
}
