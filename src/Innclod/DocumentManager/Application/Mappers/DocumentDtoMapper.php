<?php

namespace DocumentManager\Application\Mappers;

use DocumentManager\Domain\Dto\DocumentDto;
use Jhan\Kernel\Application\Mappers\BaseMapper;

class DocumentDtoMapper extends BaseMapper
{
    protected function getNewDto(): DocumentDto
    {
        return new DocumentDto();
    }

    public function createFromDbRecord(object $dbRecord): DocumentDto
    {
        $dto = $this->getNewDto();
        $dto->id = $dbRecord->id;
        $dto->name = $dbRecord->name;
        $dto->code = $dbRecord->code;
        $dto->body = $dbRecord->body;
        $dto->documentTypeId = $dbRecord->document_type_id;
        $dto->documentTypeName = $dbRecord->document_type_name;
        $dto->documentTypeCode = $dbRecord->document_type_code;
        $dto->processId = $dbRecord->process_id;
        $dto->processName = $dbRecord->process_name;
        $dto->processCode = $dbRecord->process_code;
        return $dto;
    }

}
