<?php

namespace DocumentManager\Application\Mappers;

use DocumentManager\Domain\Dto\DocumentNewDto;
use Illuminate\Http\Request;
use Jhan\Kernel\Application\Mappers\BaseMapper;

class DocumentNewDtoMapper extends BaseMapper
{
    protected function getNewDto(): DocumentNewDto
    {
        return new DocumentNewDto();
    }

    public function createFromRequest(Request $request): DocumentNewDto
    {
        $dto = $this->getNewDto();
        $dto->name = $request->get('name');
        $dto->body = $request->get('body');
        $dto->documentTypeId = $request->get('document_type_id');
        $dto->processId = $request->get('process_id');
        return $dto;
    }

}
