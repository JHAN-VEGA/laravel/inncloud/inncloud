<?php

namespace DocumentManager\Application\Mappers;

use DocumentManager\Domain\Dto\DocumentUpdateDto;
use Illuminate\Http\Request;
use Jhan\Kernel\Application\Mappers\BaseMapper;

class DocumentUpdateDtoMapper extends BaseMapper
{
    protected function getNewDto(): DocumentUpdateDto
    {
        return new DocumentUpdateDto();
    }

    public function createFromRequest(Request $request): DocumentUpdateDto
    {
        $dto = $this->getNewDto();
        $dto->name = $request->get('name');
        $dto->body = $request->get('body');
        return $dto;
    }

}
