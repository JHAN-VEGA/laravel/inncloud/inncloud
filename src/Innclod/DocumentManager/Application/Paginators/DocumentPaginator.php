<?php

namespace DocumentManager\Application\Paginators;

use DocumentManager\Application\Interfaces\Paginators\DocumentPaginatorInterface;
use DocumentManager\Domain\Dto\DocumentPaginatorDto;
use Illuminate\Support\Facades\DB;
use Jhan\Paginator\Infrastructure\Paginators\BasePaginator;

class DocumentPaginator extends BasePaginator implements DocumentPaginatorInterface
{
    protected bool $isMapItems = true;

    protected function getTableName(): string
    {
        return 'DOC_DOCUMENTO';
    }

    protected function getDatabaseConnection(): string
    {
        return 'pgsql';
    }

    public function initialize(): self
    {
        $this->query = DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->select([
                'DOC_DOCUMENTO.DOC_ID AS id',
                'DOC_DOCUMENTO.DOC_NOMBRE AS name',
                'DOC_DOCUMENTO.DOC_CODIGO AS code',
                'PRO_PROCESO.PRO_ID AS process_id',
                'PRO_PROCESO.PRO_NOMBRE AS process_name',
                'PRO_PROCESO.PRO_PREFIJO AS process_code',
                'TIP_TIPO_DOC.TIP_ID AS document_type_id',
                'TIP_TIPO_DOC.TIP_NOMBRE AS document_type_name',
                'TIP_TIPO_DOC.TIP_PREFIJO AS document_type_code',
            ])
            ->leftJoin('PRO_PROCESO', 'PRO_PROCESO.PRO_ID', '=', 'DOC_DOCUMENTO.DOC_ID_PROCESO')
            ->leftJoin('TIP_TIPO_DOC', 'TIP_TIPO_DOC.TIP_ID', '=', 'DOC_DOCUMENTO.DOC_ID_TIPO');

        return $this;
    }

    public function filterByName(string $name): self
    {
        $this->query->where('DOC_DOCUMENTO.DOC_NOMBRE', 'ilike', '%' . $name . '%');
        return $this;
    }

    public function filterByCode(string $code): self
    {
        $this->query->where('DOC_DOCUMENTO.DOC_CODIGO', 'ilike', '%'. $code .'%');
        return $this;
    }

    protected function mapItems(array $records): array
    {
        $items = [];

        foreach ($records as $record) {
            $dto = new DocumentPaginatorDto();
            $dto->id = $record->id;
            $dto->name = $record->name;
            $dto->code = $record->code;
            $dto->processId = $record->process_id;
            $dto->processName = $record->process_name;
            $dto->processCode = $record->process_code;
            $dto->documentTypeId = $record->document_type_id;
            $dto->documentTypeName = $record->document_type_name;
            $dto->documentTypeCode = $record->document_type_code;
            $items[] = $dto;
        }

        return $items;
    }

}
