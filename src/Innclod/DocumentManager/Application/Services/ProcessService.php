<?php

namespace DocumentManager\Application\Services;

use DocumentManager\Application\Interfaces\Services\ProcessServiceInterface;
use DocumentManager\Domain\Dto\ProcessDto;
use DocumentManager\Domain\Exceptions\ProcessNotFoundException;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;
use Illuminate\Support\Facades\App;

class ProcessService implements ProcessServiceInterface
{
    private ProcessRepositoryInterface $processRepo;

    public function __construct()
    {
        $this->processRepo = App::make(ProcessRepositoryInterface::class);
    }

    public function getById(int $id): ProcessDto
    {
        $record = $this->processRepo->getById($id);

        throw_if(is_null($record), new ProcessNotFoundException());

        return $record;
    }
}
