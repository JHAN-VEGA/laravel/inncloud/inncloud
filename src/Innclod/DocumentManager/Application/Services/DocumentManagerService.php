<?php

namespace DocumentManager\Application\Services;

use DocumentManager\Application\Interfaces\Services\DocumentManagerServiceInterface;
use DocumentManager\Application\Interfaces\Services\DocumentManagerValidateServiceInterface;
use DocumentManager\Application\Interfaces\Services\DocumentTypeServiceInterface;
use DocumentManager\Application\Interfaces\Services\ProcessServiceInterface;
use DocumentManager\Domain\Dto\DocumentDto;
use DocumentManager\Domain\Dto\DocumentNewDto;
use DocumentManager\Domain\Dto\DocumentUpdateDto;
use DocumentManager\Domain\Exceptions\DocumentNotFoundException;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentRepositoryInterface;
use Illuminate\Support\Facades\App;

class DocumentManagerService implements DocumentManagerServiceInterface
{
    private DocumentRepositoryInterface $documentRepo;

    private DocumentTypeServiceInterface $documentTypeService;

    protected ProcessServiceInterface $processService;

    public function __construct()
    {
        $this->documentRepo = App::make(DocumentRepositoryInterface::class);
        $this->documentTypeService = App::make(DocumentTypeServiceInterface::class);
        $this->processService = App::make(ProcessServiceInterface::class);
    }

    public function getById(int $id):DocumentDto
    {
        $document = $this->documentRepo->getById($id);

        throw_if(is_null($document), new DocumentNotFoundException());

        return $document;
    }

    public function store(DocumentNewDto $dto): int
    {
        (App::make(DocumentManagerValidateServiceInterface::class))
            ->validateStore($dto);

        $process = $this->processService->getById($dto->processId);
        $documentType = $this->documentTypeService->getById($dto->documentTypeId);

        $dto->code = $documentType->code . '-' . $process->code . '-' . $this->documentRepo
                ->getCountByProcessIdAndDocumentTypeId($process->id, $documentType->id) + 1;

        return $this->documentRepo
            ->setUser(auth()->user())
            ->store($dto);
    }

    public function update(DocumentUpdateDto $dto): self
    {
        $this->getById($dto->id);

        $this->documentRepo
            ->setUser(auth()->user())
            ->update($dto);

        return $this;
    }

    public function delete(int $id): self
    {
        $this->getById($id);

        $this->documentRepo
            ->setUser(auth()->user())
            ->delete($id);

        return $this;
    }

}
