<?php

namespace DocumentManager\Application\Services;

use DocumentManager\Application\Interfaces\Services\DocumentTypeServiceInterface;
use DocumentManager\Domain\Dto\DocumentTypeDto;
use DocumentManager\Domain\Exceptions\DocumentTypeNotFoundException;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use Illuminate\Support\Facades\App;

class DocumentTypeService implements DocumentTypeServiceInterface
{
    private DocumentTypeRepositoryInterface $documentTypeRepo;

    public function __construct()
    {
        $this->documentTypeRepo = App::make(DocumentTypeRepositoryInterface::class);
    }

    public function getById(int $id):DocumentTypeDto
    {
        $record = $this->documentTypeRepo->getById($id);

        throw_if(is_null($record), new DocumentTypeNotFoundException());

        return $record;
    }

}
