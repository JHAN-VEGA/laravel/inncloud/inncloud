<?php

namespace DocumentManager\Application\Services;

use DocumentManager\Application\Interfaces\Services\DocumentManagerValidateServiceInterface;
use DocumentManager\Domain\Dto\DocumentNewDto;
use DocumentManager\Domain\Dto\DocumentUpdateDto;
use DocumentManager\Domain\Exceptions\CustomValidationException;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use DocumentManager\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;
use Illuminate\Support\Facades\App;

class DocumentManagerValidateService implements DocumentManagerValidateServiceInterface
{
    private DocumentTypeRepositoryInterface $documentTypeRepo;

    private ProcessRepositoryInterface $processRepo;

    private array $errors = [];

    public function __construct()
    {
        $this->documentTypeRepo = App::make(DocumentTypeRepositoryInterface::class);
        $this->processRepo = App::make(ProcessRepositoryInterface::class);
    }

    public function validateStore(DocumentNewDto $dto):void
    {
        if (!$this->documentTypeRepo->existById($dto->documentTypeId)) {
            $this->errors['document_type_id'] = 'Tipo de documento no encontrado en el sistema,';
        }

        if (!$this->processRepo->existById($dto->processId)) {
            $this->errors['process_id'] = 'Proceso no encontrado en el sistema.';
        }

        throw_if(!empty($this->errors), new CustomValidationException(errors: $this->errors));
    }
}
