<?php

namespace DocumentManager\Application\Interfaces\Paginators;

use Jhan\Paginator\Infrastructure\Interfaces\BasePaginatorInterface;

interface DocumentPaginatorInterface extends BasePaginatorInterface
{
    public function filterByName(string $name):self;

    public function filterByCode(string $code):self;
}
