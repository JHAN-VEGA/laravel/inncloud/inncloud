<?php

namespace DocumentManager\Application\Interfaces\Services;

use DocumentManager\Domain\Dto\DocumentTypeDto;

interface DocumentTypeServiceInterface
{
    public function getById(int $id):DocumentTypeDto;
}
