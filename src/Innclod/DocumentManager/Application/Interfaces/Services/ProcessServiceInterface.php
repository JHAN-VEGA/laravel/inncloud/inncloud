<?php

namespace DocumentManager\Application\Interfaces\Services;

use DocumentManager\Domain\Dto\ProcessDto;

interface ProcessServiceInterface
{
    public function getById(int $id): ProcessDto;
}
