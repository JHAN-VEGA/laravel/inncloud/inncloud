<?php

namespace DocumentManager\Application\Interfaces\Services;

use DocumentManager\Domain\Dto\DocumentDto;
use DocumentManager\Domain\Dto\DocumentNewDto;
use DocumentManager\Domain\Dto\DocumentUpdateDto;

interface DocumentManagerServiceInterface
{
    public function getById(int $id):DocumentDto;

    public function store(DocumentNewDto $dto):int;

    public function update(DocumentUpdateDto $dto):self;

    public function delete(int $id):self;
}
