<?php

namespace DocumentManager\Application\Interfaces\Services;

use DocumentManager\Domain\Dto\DocumentNewDto;
use DocumentManager\Domain\Dto\DocumentUpdateDto;

interface DocumentManagerValidateServiceInterface
{
    public function validateStore(DocumentNewDto $dto):void;
}
