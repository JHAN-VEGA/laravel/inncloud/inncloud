<?php

namespace AuthManager\Application\Interfaces\Services;

use AuthManager\Domain\Dto\LoginDto;

interface AuthManagerServiceInterface
{
    public function login(LoginDto $dto):self;
}
