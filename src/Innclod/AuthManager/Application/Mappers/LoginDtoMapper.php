<?php

namespace AuthManager\Application\Mappers;

use AuthManager\Domain\Dto\LoginDto;
use Illuminate\Http\Request;
use Jhan\Kernel\Application\Mappers\BaseMapper;

class LoginDtoMapper extends BaseMapper
{
    public function createFromRequest(Request $request): LoginDto
    {
        $dto = $this->getNewDto();
        $dto->email = $request->get('email');
        $dto->password = $request->get('password');
        return $dto;
    }

    protected function getNewDto(): LoginDto
    {
        return new LoginDto();
    }
}
