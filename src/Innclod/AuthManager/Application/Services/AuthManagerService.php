<?php

namespace AuthManager\Application\Services;

use AuthManager\Application\Interfaces\Services\AuthManagerServiceInterface;
use AuthManager\Domain\Dto\LoginDto;
use AuthManager\Domain\Exceptions\InvalidCredentialException;
use AuthManager\Domain\Exceptions\UnauthorizedActionException;
use AuthManager\Domain\Exceptions\UserNotFoundException;
use AuthManager\Infrastructure\ServiceLayer\Interfaces\Repositories\UserRepositoryInterface;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class AuthManagerService implements AuthManagerServiceInterface
{
    private UserRepositoryInterface $userRepo;

    public function __construct()
    {
        $this->userRepo = App::make(UserRepositoryInterface::class);
    }

    public function login(LoginDto $dto):self
    {
        $user = $this->userRepo
            ->getByEmail($dto->email);

        throw_if(is_null($user), new UserNotFoundException());

        throw_if(!$user->active, new UnauthorizedActionException());

        throw_if(!Hash::check($dto->password, $user->password), new InvalidCredentialException());

        auth()->login($user);

        return $this;
    }

}
