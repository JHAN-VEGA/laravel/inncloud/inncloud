<?php

namespace AuthManager\Domain\Dto;

use Jhan\Kernel\Domain\Dto\BaseDto;

class LoginDto extends BaseDto
{
    public string $email;
    public string $password;
}
