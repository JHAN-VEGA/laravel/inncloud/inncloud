<?php

namespace AuthManager\Domain\Exceptions;

use Jhan\Kernel\Domain\Exceptions\BaseException;

class UnauthorizedActionException extends BaseException
{
    protected $message = 'Acción no autorizada';
    protected $code = 401;

}
