<?php

namespace AuthManager\Domain\Exceptions;

use Jhan\Kernel\Domain\Exceptions\BaseException;

class InvalidCredentialException extends BaseException
{
    protected $message = 'Credenciales invalidas';
    protected $code = 401;
}
