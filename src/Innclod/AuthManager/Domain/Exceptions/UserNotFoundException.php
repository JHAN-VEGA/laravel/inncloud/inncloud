<?php

namespace AuthManager\Domain\Exceptions;

use Jhan\Kernel\Domain\Exceptions\BaseException;

class UserNotFoundException extends BaseException
{
    protected $message = 'Usuario no encontrado en el sistema';
    protected $code = 404;
}
