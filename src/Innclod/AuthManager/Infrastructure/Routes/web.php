<?php

use AuthManager\Infrastructure\ServiceLayer\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

Route::middleware(['web'])->group(function () {

    Route::post('auth', [AuthController::class, 'auth'])
        ->name('auth');

    Route::post('logout', [AuthController::class, 'logout'])
        ->middleware(['auth'])
        ->name('logout');
});
