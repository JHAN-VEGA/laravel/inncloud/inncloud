<?php

namespace AuthManager\Infrastructure\ServiceLayer\Interfaces\Repositories;

use App\Models\User;
use Jhan\Kernel\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
    public function getByEmail(string $email):?User;
}
