<?php

namespace AuthManager\Infrastructure\ServiceLayer;

use AuthManager\Application\Interfaces\Services\AuthManagerServiceInterface;
use AuthManager\Application\Services\AuthManagerService;
use AuthManager\Infrastructure\ServiceLayer\Interfaces\Repositories\UserRepositoryInterface;
use AuthManager\Infrastructure\ServiceLayer\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class AuthManagerServiceProvider extends ServiceProvider
{
    public function register():void
    {
        $this->registerRoutes();
        $this->registerBinds();
        $this->registerMiddlewares();
    }

    public function registerRoutes():void
    {
        $this->loadRoutesFrom(__DIR__.'/../Routes/web.php');
    }

    public function registerBinds():void
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(AuthManagerServiceInterface::class, AuthManagerService::class);
    }

    public function registerMiddlewares():void
    {
        $router = $this->app['router'];
        /*$router->aliasMiddleware('jwt', \TFS\AuthManagerService\Infrastructure\ServiceLayer\Middlewares\JwtMiddleware::class);*/
    }
}
