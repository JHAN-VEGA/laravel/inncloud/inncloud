<?php

namespace AuthManager\Infrastructure\ServiceLayer\Controllers;

use AuthManager\Application\Interfaces\Services\AuthManagerServiceInterface;
use AuthManager\Application\Mappers\LoginDtoMapper;
use Illuminate\Http\Request;
use Jhan\Http\Infrastructure\Controllers\BaseController;

class AuthController extends BaseController
{
    private AuthManagerServiceInterface $authManagerService;

    public function __construct(
        AuthManagerServiceInterface $authManagerService
    )
    {
        $this->authManagerService = $authManagerService;
    }

    public function login()
    {
        if (is_null(auth()->user()))
            return view('Innclod.AuthManager.login');
        else
            return redirect()->route('dashboard');
    }

    public function auth(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required', 'string'],
        ]);

        $this->authManagerService
            ->login((new LoginDtoMapper())->createFromRequest($request));

        return redirect()->route('dashboard');
    }

    public function logout()
    {
        return $this->execWithHttpResponse(function () {
            auth()->logout();
            return redirect()->route('login');
        });
    }
}
