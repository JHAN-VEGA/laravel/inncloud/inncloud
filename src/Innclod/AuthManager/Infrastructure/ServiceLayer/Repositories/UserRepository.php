<?php

namespace AuthManager\Infrastructure\ServiceLayer\Repositories;

use App\Models\User;
use AuthManager\Infrastructure\ServiceLayer\Interfaces\Repositories\UserRepositoryInterface;
use Jhan\Kernel\Infrastructure\Repositories\BaseRepository;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function getTableName(): string
    {
        return 'users';
    }

    public function getDatabaseConnection(): string
    {
        return 'pgsql';
    }

    public function getByEmail(string $email):?User
    {
        $user = User::query()
            ->select(['id', 'name', 'email', 'active', 'password'])
            ->where('email', '=', $email)
            ->first();

        if (is_null($user)) return null;

        return $user;
    }

}
