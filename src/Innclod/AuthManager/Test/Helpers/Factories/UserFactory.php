<?php

namespace AuthManager\Test\Helpers\Factories;

class UserFactory extends BaseFactory
{
    public function getTableName(): string
    {
        return 'users';
    }

    public function getDatabaseConnection(): string
    {
        return 'pgsql';
    }

    protected function getAttributes(): array
    {
        return [
            'name' => fake()->name(),
            'email' => fake()->email(),
            'password' => bcrypt(fake()->password()),
            'active' => true,
        ];
    }

}
