<?php

namespace AuthManager\Test\Infrastructure\Controllers\AuthController;

use AuthManager\Test\BaseTest;

class LoginTest extends BaseTest
{
    public function testIsLoginWorking()
    {
        $response = $this->get(route('login'));
        $response->assertViewIs('Innclod.AuthManager.login');
        $response->assertSee('Innclod.AuthManager.login');
        $response->assertStatus(200);
    }

    public function testIsLoginWithAuthWorking()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('login'));
        $response->assertRedirectToRoute('dashboard');
        $response->assertStatus(302);
    }
}
