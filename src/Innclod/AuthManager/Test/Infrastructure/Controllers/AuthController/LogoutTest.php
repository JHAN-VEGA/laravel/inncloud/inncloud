<?php

namespace AuthManager\Test\Infrastructure\Controllers\AuthController;

use AuthManager\Test\BaseTest;

class LogoutTest extends BaseTest
{
    public function testIsLogoutWorking()
    {
        $this->actingAs($this->user);
        $response = $this->post(route('logout'));
        $response->assertStatus(302);
        $response->assertRedirectToRoute('login');
    }
}
