<?php

namespace AuthManager\Test\Infrastructure\Controllers\AuthController;

use AuthManager\Application\Interfaces\Services\AuthManagerServiceInterface;
use AuthManager\Test\BaseTest;

class AuthTest extends BaseTest
{
    public function testIsAuthWorking()
    {
        $authManagerServiceMock = \Mockery::mock(AuthManagerServiceInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $authManagerServiceMock->shouldReceive('login')
            ->once()
            ->andReturnSelf();

        $this->instance(AuthManagerServiceInterface::class, $authManagerServiceMock);

        $response = $this->post(route('auth'), [
            'email' => 'email@innclod.com',
            'password' => 'password',
        ]);

        $response->assertStatus(302);
        $response->assertRedirectToRoute('dashboard');
    }
}
