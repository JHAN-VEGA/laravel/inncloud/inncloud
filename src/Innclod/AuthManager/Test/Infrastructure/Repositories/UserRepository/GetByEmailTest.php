<?php

namespace AuthManager\Test\Infrastructure\Repositories\UserRepository;

use App\Models\User;
use AuthManager\Infrastructure\ServiceLayer\Interfaces\Repositories\UserRepositoryInterface;
use AuthManager\Test\BaseTest;
use AuthManager\Test\Helpers\Factories\UserFactory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class GetByEmailTest extends BaseTest
{
    use DatabaseTransactions;

    public function testIsGetByEmailWorking()
    {
        $this->getNewUserId();
        $user = (App::make(UserRepositoryInterface::class))
            ->getByEmail('email@innclod.com');

        $this->assertNotNull($user);
        $this->assertInstanceOf(User::class, $user);
    }


    public function testIsGetByEmailFailing()
    {
        $user = (App::make(UserRepositoryInterface::class))
            ->getByEmail('email@innclod.com');

        $this->assertNull($user);
    }

    protected function getNewUserId():int
    {
        $factory = new UserFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get([
                'email' => 'email@innclod.com',
            ]));
    }
}
