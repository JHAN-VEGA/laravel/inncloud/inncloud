<?php

namespace AuthManager\Test\Application\Services\AuthManagerService;

use App\Models\User;
use AuthManager\Application\Interfaces\Services\AuthManagerServiceInterface;
use AuthManager\Domain\Dto\LoginDto;
use AuthManager\Domain\Exceptions\InvalidCredentialException;
use AuthManager\Domain\Exceptions\UnauthorizedActionException;
use AuthManager\Domain\Exceptions\UserNotFoundException;
use AuthManager\Infrastructure\ServiceLayer\Interfaces\Repositories\UserRepositoryInterface;
use AuthManager\Test\BaseTest;
use Illuminate\Support\Facades\App;

class LoginTest extends BaseTest
{
    public function testIsLoginWorking()
    {
        $userRepoMock = \Mockery::mock(UserRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $userRepoMock->shouldReceive('getByEmail')
            ->once()
            ->andReturnUsing(function () {
                $user = new User();
                $user->id = 1;
                $user->name = 'Nombre';
                $user->email = 'email@innclod.com';
                $user->password = bcrypt('password');
                $user->active = true;
                return $user;
            });

        $this->instance(UserRepositoryInterface::class, $userRepoMock);

        $dto = new LoginDto();
        $dto->email = 'email@innclod.com';
        $dto->password = 'password';

        (App::make(AuthManagerServiceInterface::class))
            ->login($dto);

        $this->assertNotNull(auth()->user());
    }

    public function testIsLoginNullFailing()
    {
        $userRepoMock = \Mockery::mock(UserRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $userRepoMock->shouldReceive('getByEmail')
            ->once()
            ->andReturnNull();

        $this->instance(UserRepositoryInterface::class, $userRepoMock);

        try {
            $dto = new LoginDto();
            $dto->email = 'email@innclod.com';
            $dto->password = 'password';

            (App::make(AuthManagerServiceInterface::class))
                ->login($dto);
        }catch (UserNotFoundException $exception) {
            $this->assertEquals(404, $exception->getCode());
        }
    }

    public function testIsLoginUnauthorizedFailing()
    {
        $userRepoMock = \Mockery::mock(UserRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $userRepoMock->shouldReceive('getByEmail')
            ->once()
            ->andReturnUsing(function () {
                $user = new User();
                $user->id = 1;
                $user->name = 'Nombre';
                $user->email = 'email@innclod.com';
                $user->password = bcrypt('password');
                $user->active = false;
                return $user;
            });
        $this->instance(UserRepositoryInterface::class, $userRepoMock);

        try {
            $dto = new LoginDto();
            $dto->email = 'email@innclod.com';
            $dto->password = 'password';

            (App::make(AuthManagerServiceInterface::class))
                ->login($dto);
        }catch (UnauthorizedActionException $exception) {
            $this->assertEquals(401, $exception->getCode());
        }
    }

    public function testIsLoginInvalidCredentialFailing()
    {
        $userRepoMock = \Mockery::mock(UserRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $userRepoMock->shouldReceive('getByEmail')
            ->once()
            ->andReturnUsing(function () {
                $user = new User();
                $user->id = 1;
                $user->name = 'Nombre';
                $user->email = 'email@innclod.com';
                $user->password = bcrypt('invalid');
                $user->active = true;
                return $user;
            });
        $this->instance(UserRepositoryInterface::class, $userRepoMock);

        try {
            $dto = new LoginDto();
            $dto->email = 'email@innclod.com';
            $dto->password = 'password';

            (App::make(AuthManagerServiceInterface::class))
                ->login($dto);
        }catch (InvalidCredentialException $exception) {
            $this->assertEquals(401, $exception->getCode());
        }
    }

}
