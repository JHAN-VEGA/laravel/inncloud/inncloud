<?php

namespace Master\Test\Helpers\Factories;

abstract class BaseFactory
{
    abstract public function getTableName():string;

    abstract public function getDatabaseConnection():string;

    abstract protected function getAttributes():array;

    public function get(array $attributes = []):array
    {
        return array_merge(
            $this->getAttributes(), $attributes
        );
    }
}
