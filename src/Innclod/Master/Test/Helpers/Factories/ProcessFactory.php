<?php

namespace Master\Test\Helpers\Factories;

use Illuminate\Support\Str;

class ProcessFactory extends BaseFactory
{
    public function getTableName(): string
    {
        return 'PRO_PROCESO';
    }

    public function getDatabaseConnection(): string
    {
        return 'pgsql';
    }

    protected function getAttributes(): array
    {
        return [
            'PRO_NOMBRE' => Str::random(10),
            'PRO_PREFIJO' => Str::random(5),
        ];
    }

}
