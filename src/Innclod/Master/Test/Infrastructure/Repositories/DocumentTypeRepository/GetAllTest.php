<?php

namespace Master\Test\Infrastructure\Repositories\DocumentTypeRepository;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Master\Domain\Dto\DocumentTypeDto;
use Master\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use Master\Test\BaseTest;
use Master\Test\Helpers\Factories\DocumentTypeFactory;

class GetAllTest extends BaseTest
{
    use DatabaseTransactions;

    public function testIsGetAllWorking()
    {
        $this->getNewDocumentTypeId();

        $records = (App::make(DocumentTypeRepositoryInterface::class))
            ->getAll();

        $this->assertInstanceOf(DocumentTypeDto::class, $records[0]);
    }

    protected function getNewDocumentTypeId():int
    {
        $factory = new DocumentTypeFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get(), 'TIP_ID');
    }

}
