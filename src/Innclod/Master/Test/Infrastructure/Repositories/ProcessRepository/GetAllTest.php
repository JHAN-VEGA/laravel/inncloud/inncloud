<?php

namespace Master\Test\Infrastructure\Repositories\ProcessRepository;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Master\Domain\Dto\ProcessDto;
use Master\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;
use Master\Test\BaseTest;
use Master\Test\Helpers\Factories\ProcessFactory;

class GetAllTest extends BaseTest
{
    use DatabaseTransactions;

    public function testIsGetAllWorking()
    {
        $this->getNewProcessId();

        $records = (App::make(ProcessRepositoryInterface::class))
            ->getAll();

        $this->assertInstanceOf(ProcessDto::class, $records[0]);
    }

    protected function getNewProcessId():int
    {
        $factory = new ProcessFactory();
        return DB::connection($factory->getDatabaseConnection())
            ->table($factory->getTableName())
            ->insertGetId($factory->get(), 'PRO_ID');
    }

}
