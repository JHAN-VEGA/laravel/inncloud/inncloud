<?php

namespace Master\Test\Infrastructure\Controllers\DocumentTypeController;

use Master\Application\Interfaces\Services\DocumentTypeServiceInterface;
use Master\Domain\Dto\DocumentTypeDto;
use Master\Test\BaseTest;

class GetAllTest extends BaseTest
{
    public function testIsGetAllWorking()
    {
        $this->actingAs($this->user);

        $tipTipoDocServiceMock = \Mockery::mock(DocumentTypeServiceInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $tipTipoDocServiceMock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $dto = new DocumentTypeDto();
                $dto->id = 1;
                $dto->name = 'Tipo de documento';
                $dto->code = 'TD';
                return [$dto];
            });

        $this->instance(DocumentTypeServiceInterface::class, $tipTipoDocServiceMock);

        $response = $this->get(route('documentTypes.getAll'));

        $response->assertJsonStructure(['success', 'code', 'message', 'data']);
        $this->assertCount(1, $response->json()['data']);
        $response->assertStatus(200);
        $this->assertTrue(true);
    }
}
