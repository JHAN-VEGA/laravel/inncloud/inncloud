<?php

namespace Master\Test\Infrastructure\Controllers\ProcessController;

use Master\Application\Interfaces\Services\ProcessServiceInterface;
use Master\Domain\Dto\ProcessDto;
use Master\Test\BaseTest;

class GetAllTest extends BaseTest
{
    public function testIsGetAllWorking()
    {
        $this->actingAs($this->user);

        $proProcesoServiceMock = \Mockery::mock(ProcessServiceInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $proProcesoServiceMock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $dto = new ProcessDto();
                $dto->id = 1;
                $dto->name = 'Proceso';
                $dto->code = 'PR';
                return [$dto];
            });

        $this->instance(ProcessServiceInterface::class, $proProcesoServiceMock);

        $response = $this->get(route('process.getAll'));

        $response->assertJsonStructure(['success', 'code', 'message', 'data']);
        $this->assertCount(1, $response->json()['data']);
        $response->assertStatus(200);
        $this->assertTrue(true);
    }
}
