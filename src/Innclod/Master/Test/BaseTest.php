<?php

namespace Master\Test;

use App\Models\User;
use Tests\TestCase;

class BaseTest extends TestCase
{
    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = new User();
        $this->user->id = 1;
        $this->user->name = 'Administrador';
        $this->user->email = 'admin@innclod.com';
        $this->user->active = true;
    }

    public function testIsWorking()
    {
        $this->assertTrue(true);
    }
}
