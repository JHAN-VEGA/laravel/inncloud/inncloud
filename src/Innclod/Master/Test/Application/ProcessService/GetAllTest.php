<?php

namespace Master\Test\Application\ProcessService;

use Illuminate\Support\Facades\App;
use Master\Application\Interfaces\Services\ProcessServiceInterface;
use Master\Domain\Dto\ProcessDto;
use Master\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;
use Master\Test\BaseTest;

class GetAllTest extends BaseTest
{
    public function testIsGetAllWorking()
    {
        $proProcesoRepoMock = \Mockery::mock(ProcessRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $proProcesoRepoMock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $dto = new ProcessDto();
                $dto->id = 1;
                $dto->name = 'Proceso';
                $dto->code = 'PR';
                return [$dto];
            });

        $this->instance(ProcessRepositoryInterface::class, $proProcesoRepoMock);

        $records = (App::make(ProcessServiceInterface::class))
            ->getAll();

        $this->assertCount(1, $records);
        $this->assertInstanceOf(ProcessDto::class, $records[0]);
    }
}
