<?php

namespace Master\Test\Application\DocumentTypeService;

use Illuminate\Support\Facades\App;
use Master\Application\Interfaces\Services\DocumentTypeServiceInterface;
use Master\Domain\Dto\DocumentTypeDto;
use Master\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use Master\Test\BaseTest;

class GetAllTest extends BaseTest
{
    public function testIsGetAllWorking()
    {
        $tipTipoDocRepoMock = \Mockery::mock(DocumentTypeRepositoryInterface::class)
            ->shouldAllowMockingProtectedMethods();

        $tipTipoDocRepoMock->shouldReceive('getAll')
            ->once()
            ->andReturnUsing(function () {
                $dto = new DocumentTypeDto();
                $dto->id = 1;
                $dto->name = 'Tipo documento';
                $dto->code = 'TP';
                return [$dto];
            });

        $this->instance(DocumentTypeRepositoryInterface::class, $tipTipoDocRepoMock);

        $records = (App::make(DocumentTypeServiceInterface::class))
            ->getAll();

        $this->assertCount(1, $records);
        $this->assertInstanceOf(DocumentTypeDto::class, $records[0]);
    }
}
