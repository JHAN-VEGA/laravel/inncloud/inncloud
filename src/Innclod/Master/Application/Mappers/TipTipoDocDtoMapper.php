<?php

namespace Master\Application\Mappers;

use Jhan\Kernel\Application\Mappers\BaseMapper;
use Master\Domain\Dto\DocumentTypeDto;

class TipTipoDocDtoMapper extends BaseMapper
{
    protected function getNewDto(): DocumentTypeDto
    {
        return new DocumentTypeDto();
    }

    public function createFromDbRecord(object $dbRecord):DocumentTypeDto
    {
        $dto = $this->getNewDto();
        $dto->id = $dbRecord->id;
        $dto->name = $dbRecord->name;
        $dto->code = $dbRecord->code;
        return $dto;
    }
}
