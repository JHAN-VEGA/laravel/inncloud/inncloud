<?php

namespace Master\Application\Mappers;

use Jhan\Kernel\Application\Mappers\BaseMapper;
use Master\Domain\Dto\ProcessDto;

class ProProcesoDtoMapper extends BaseMapper
{
    protected function getNewDto(): ProcessDto
    {
        return new ProcessDto();
    }

    public function createFromDbRecord(object $dbRecord):ProcessDto
    {
        $dto = $this->getNewDto();
        $dto->id = $dbRecord->id;
        $dto->name = $dbRecord->name;
        $dto->code = $dbRecord->code;
        return $dto;
    }
}
