<?php

namespace Master\Application\Services;

use Illuminate\Support\Facades\App;
use Master\Application\Interfaces\Services\DocumentTypeServiceInterface;
use Master\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;

class DocumentTypeService implements DocumentTypeServiceInterface
{
    private DocumentTypeRepositoryInterface $tipTipoDocRepo;

    public function __construct()
    {
        $this->tipTipoDocRepo = App::make(DocumentTypeRepositoryInterface::class);
    }

    public function getAll(): array
    {
        return $this->tipTipoDocRepo
            ->getAll();
    }

}
