<?php

namespace Master\Application\Services;

use Illuminate\Support\Facades\App;
use Master\Application\Interfaces\Services\ProcessServiceInterface;
use Master\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;

class ProcessService implements ProcessServiceInterface
{
    private ProcessRepositoryInterface $proProcesoRepo;

    public function __construct()
    {
        $this->proProcesoRepo = App::make(ProcessRepositoryInterface::class);
    }

    public function getAll(): array
    {
        return $this->proProcesoRepo
            ->getAll();
    }

}
