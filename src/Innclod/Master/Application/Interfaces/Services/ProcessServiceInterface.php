<?php

namespace Master\Application\Interfaces\Services;

interface ProcessServiceInterface
{
    public function getAll():array;
}
