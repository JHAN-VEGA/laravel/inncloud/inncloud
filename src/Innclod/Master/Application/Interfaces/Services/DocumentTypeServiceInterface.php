<?php

namespace Master\Application\Interfaces\Services;

interface DocumentTypeServiceInterface
{
    public function getAll():array;
}
