<?php

use Illuminate\Support\Facades\Route;
use Master\Infrastructure\ServiceLayer\Controllers\ProcessController;
use Master\Infrastructure\ServiceLayer\Controllers\DocumentTypeController;

Route::prefix('tip_tipo_doc')->middleware(['web', 'auth'])->group(function () {
    Route::get('get_all', [DocumentTypeController::class, 'getAll'])->name('documentTypes.getAll');
});

Route::prefix('pro_proceso')->middleware(['web', 'auth'])->group(function () {
    Route::get('get_all', [ProcessController::class, 'getAll'])->name('process.getAll');
});
