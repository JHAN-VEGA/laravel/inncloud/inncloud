<?php

namespace Master\Infrastructure\ServiceLayer\Repositores;

use Illuminate\Support\Facades\DB;
use Jhan\Kernel\Infrastructure\Repositories\BaseRepository;
use Master\Application\Mappers\ProProcesoDtoMapper;
use Master\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;

class ProcessRepository extends BaseRepository implements  ProcessRepositoryInterface
{
    public function getTableName(): string
    {
        return 'PRO_PROCESO';
    }

    public function getDatabaseConnection(): string
    {
        return 'pgsql';
    }

    public function getAll(): array
    {
        $records = DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->select([
                'PRO_ID AS id',
                'PRO_NOMBRE as name',
                'PRO_PREFIJO as code'
            ])->get();

        return (new ProProcesoDtoMapper())
            ->createFromDbRecords($records);
    }

}
