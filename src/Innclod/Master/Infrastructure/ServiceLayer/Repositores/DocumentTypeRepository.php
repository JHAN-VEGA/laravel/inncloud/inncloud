<?php

namespace Master\Infrastructure\ServiceLayer\Repositores;

use Illuminate\Support\Facades\DB;
use Jhan\Kernel\Infrastructure\Repositories\BaseRepository;
use Master\Application\Mappers\TipTipoDocDtoMapper;
use Master\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;

class DocumentTypeRepository extends BaseRepository implements  DocumentTypeRepositoryInterface
{
    public function getTableName(): string
    {
        return 'TIP_TIPO_DOC';
    }

    public function getDatabaseConnection(): string
    {
        return 'pgsql';
    }

    public function getAll(): array
    {
        $records = DB::connection($this->getDatabaseConnection())
            ->table($this->getTableName())
            ->select([
                'TIP_ID AS id',
                'TIP_NOMBRE as name',
                'TIP_PREFIJO as code'
            ])->get();

        return (new TipTipoDocDtoMapper())
            ->createFromDbRecords($records);
    }

}
