<?php

namespace Master\Infrastructure\ServiceLayer\Interfaces\Repositories;

use Jhan\Kernel\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

interface DocumentTypeRepositoryInterface extends BaseRepositoryInterface
{
    public function getAll():array;
}
