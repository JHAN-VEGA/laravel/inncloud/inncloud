<?php

namespace Master\Infrastructure\ServiceLayer\Interfaces\Repositories;

use Jhan\Kernel\Infrastructure\Interfaces\Repositories\BaseRepositoryInterface;

interface ProcessRepositoryInterface extends BaseRepositoryInterface
{
    public function getAll():array;
}
