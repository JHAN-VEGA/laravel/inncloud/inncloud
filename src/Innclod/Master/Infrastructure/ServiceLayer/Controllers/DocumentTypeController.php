<?php

namespace Master\Infrastructure\ServiceLayer\Controllers;

use Jhan\Http\Infrastructure\Controllers\BaseController;
use Master\Application\Interfaces\Services\DocumentTypeServiceInterface;

class DocumentTypeController extends BaseController
{
    private DocumentTypeServiceInterface $tipTipoDocService;

    public function __construct(
        DocumentTypeServiceInterface $tipTipoDocService
    )
    {
        $this->tipTipoDocService = $tipTipoDocService;
    }

    public function getAll()
    {
        return $this->execWithJsonSuccessResponse(function () {
            return [
                'message' => 'Listado de tipos de documento',
                'data' => $this->tipTipoDocService->getAll()
            ];
        });
    }
}
