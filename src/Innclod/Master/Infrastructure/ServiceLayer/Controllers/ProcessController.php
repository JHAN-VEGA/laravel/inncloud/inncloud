<?php

namespace Master\Infrastructure\ServiceLayer\Controllers;

use Jhan\Http\Infrastructure\Controllers\BaseController;
use Master\Application\Interfaces\Services\ProcessServiceInterface;

class ProcessController extends BaseController
{
    private ProcessServiceInterface $proProcesoService;

    public function __construct(
        ProcessServiceInterface $proProcesoService
    )
    {
        $this->proProcesoService = $proProcesoService;
    }

    public function getAll()
    {
        return $this->execWithJsonSuccessResponse(function () {
            return [
                'message' => 'Listado de procesos',
                'data' => $this->proProcesoService->getAll(),
            ];
        });
    }
}
