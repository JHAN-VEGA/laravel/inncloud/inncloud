<?php

namespace Master\Infrastructure\ServiceLayer;

use Illuminate\Support\ServiceProvider;
use Master\Application\Interfaces\Services\ProcessServiceInterface;
use Master\Application\Interfaces\Services\DocumentTypeServiceInterface;
use Master\Application\Services\ProcessService;
use Master\Application\Services\DocumentTypeService;
use Master\Infrastructure\ServiceLayer\Interfaces\Repositories\ProcessRepositoryInterface;
use Master\Infrastructure\ServiceLayer\Interfaces\Repositories\DocumentTypeRepositoryInterface;
use Master\Infrastructure\ServiceLayer\Repositores\ProcessRepository;
use Master\Infrastructure\ServiceLayer\Repositores\DocumentTypeRepository;

class MasterServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->registerRoutes();
        $this->registerBinds();
        $this->registerMiddlewares();
    }

    public function registerRoutes()
    {
        $this->loadRoutesFrom(__DIR__.'/../Routes/web.php');
    }

    public function registerBinds()
    {
        $this->app->bind(DocumentTypeServiceInterface::class, DocumentTypeService::class);
        $this->app->bind(ProcessServiceInterface::class, ProcessService::class);
        $this->app->bind(DocumentTypeRepositoryInterface::class, DocumentTypeRepository::class);
        $this->app->bind(ProcessRepositoryInterface::class, ProcessRepository::class);
    }

    public function registerMiddlewares()
    {
        $router = $this->app['router'];
    }

}
