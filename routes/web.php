<?php

use AuthManager\Infrastructure\ServiceLayer\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

Route::middleware(['web'])->group(function () {
    Route::get('', [AuthController::class, 'login'])
        ->name('login');

    Route::get('dashboard', fn () => view('dashboard'))
        ->middleware(['auth'])
        ->name('dashboard');
});
