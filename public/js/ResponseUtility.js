var ResponseUtility= (function () {

    var processFaillRequest= function(jqXHR, textStatus, errorThrown){

        if (jqXHR.status== 419)
            parent.location.reload();
        else if(jqXHR.status==422)
            alert(_.pluck(jqXHR.responseJSON.errors, '0').join("\n"));
        else if(jqXHR.status==500)
            alert("Error de conexion con el servidor.\nRevise su conexion a internet.");
        else if(jqXHR.status==403)
            alert('Acceso denegado.');
        else if(jqXHR.status==401)
            alert('Usuario sin autorización. Revise que la sesión no haya finalizado.');
        else
            alert("No se han podido cargar los datos. Intente mas tarde.--" + textStatus);

    };

    var processFailFetchRequest= function(response){
        if (response.abort)
            parent.location.reload();
        else if('errors' in response) {
            const errors = {};
            for (const key in response.errors) {
                errors[key.replace(/(\w+)\.(\d+)\.(\w+)/g, '$1[$2][$3]')] = response.errors[key];
            }

            _.map(errors, function(err, key){
                $("[name]").removeClass('is-invalid')
                $("[validation-for]").text("");

                setTimeout(() => {
                    $("[name='"+key+"']").addClass("is-invalid");
                    $("[validation-for='"+key+"']").text(err);
                }, 50)
            });
        }
        else if('message' in  response)
            toastr.error(response.message, 'Error');
        else
            alert("No se han podido cargar los datos. Intente mas tarde.--");
    };

    var add_csrfToken= function(data){
        data.push({
            name: "_token",
            value: window.Laravel.csrfToken
        });
        return data;
    }

    function construct(){//Funcion que controla cuales son los metodos publicos
        return {
            processFaillRequest    : processFaillRequest,
            processFailFetchRequest: processFailFetchRequest,
            add_csrfToken          : add_csrfToken
        }
    }
    return {construct:construct};//retorna los metodos publicos
})().construct();
