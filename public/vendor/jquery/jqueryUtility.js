//Convierte a primer letra en mayuscula el resto en minuscula
String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
//El :contains de Jquery diferencia entre minusculas y mayusculas, esta sección crea :icontains que no hace distinción.
jQuery.expr[':'].icontains = function(a, i, m) {
    return jQuery(a).text().toUpperCase()
        .indexOf(m[3].toUpperCase()) >= 0;
};

$.fn.loading = function(label) {
    if(label==undefined)label='';
    this.each(function(index, element) {
        /* Examina si es un contenedor, en ese caso busca todos los submit dentro del mismo para ponerlos a cargar*/
        if($(element).is("div") || $(element).is("form")){
            elementsToLoading= $(element).find('[type="submit"], .submit');
        }else{
            elementsToLoading= [element];
        }
        /* Agrega la animación de cargador a cada elemento */
        $.each(elementsToLoading, function(index2, elementToLoading){
            $(elementToLoading).unloading();
            $(elementToLoading).prop('disabled', true);
            $(elementToLoading).prop('readonly', true);
            $(elementToLoading).prepend(
                '<span class="__loading__"><i class="fa fa-spinner fa-spin fa-3x fa-fw" style="font-size: 16px"></i>'+
                '<span>'+label+'</span></span>'
            );
        });
    });
};

$.fn.unloading = function() {
    this.each(function(index, element) {
        /* Examina si es un contenedor, en ese caso busca todos los submit dentro del mismo para removerles cargador*/
        if($(element).is("div") || $(element).is("form")){
            elementsToUnloading= $(element).find('[type="submit"], .submit');
        }else{
            elementsToUnloading= [element];
        }
        /* Retira la animación de cargador a cada elemento */
        $.each(elementsToUnloading, function(index2, elementToLoading){
            $(elementToLoading).find('.__loading__').remove();
            $(elementToLoading).prop('disabled', false);
            $(elementToLoading).prop('readonly', false);
        });
    });
};